//#include "Config/Config.h"
#include "Common.h"
#include "Config.h"
#include "Player.h"
#include "bp_ai.h"
#include "bp_cai.h"
#include "bp_mgr.h"
//#include "WorldPacket.h"
#include "WorldSession.h"
//#include "Chat.h"
//#include "ObjectMgr.h"
#include "GossipDef.h"
//#include "Language.h"
//#include "WaypointMovementGenerator.h"
//#include "Guild.h"
#include "Group.h"
#include "SpellInfo.h"
#include "Opcodes.h"

PlayerbotMgrOwner::PlayerbotMgrOwner(Player* const master) : m_master(master)
{
    // load config variables
    m_confFollowDistance[0] = ConfigMgr::GetFloatDefault("Bot.FollowDistanceMin", 0.5f);
    m_confFollowDistance[1] = ConfigMgr::GetFloatDefault("Bot.FollowDistanceMax", 1.0f);
    //m_confMaxNumBots = ConfigMgr::GetIntDefault("Bot.MaxPlayerbots", 9);
    //m_confDebugWhisper = ConfigMgr::GetBoolDefault("Bot.DebugWhisper", false);
    //m_confCollectCombat = ConfigMgr::GetBoolDefault("Bot.Collect.Combat", true);
    //m_confCollectQuest = ConfigMgr::GetBoolDefault("Bot.Collect.Quest", true);
    //m_confCollectProfession = ConfigMgr::GetBoolDefault("Bot.Collect.Profession", true);
    //m_confCollectLoot = ConfigMgr::GetBoolDefault("Bot.Collect.Loot", true);
    //m_confCollectSkin = ConfigMgr::GetBoolDefault("Bot.Collect.Skin", true);
    //m_confCollectObjects = ConfigMgr::GetBoolDefault("Bot.Collect.Objects", true);
    //m_confCollectDistanceMax = ConfigMgr::GetIntDefault("Bot.Collect.DistanceMax", 50);
    //gConfigSellLevelDiff = ConfigMgr::GetIntDefault("Bot.SellAll.LevelDiff", 10);
    //if (m_confCollectDistanceMax > 100)
    //{
    //    //sLog->outError("Playerbot: Bot.Collect.DistanceMax higher than allowed. Using 100");
    //    m_confCollectDistanceMax = 100;
    //}
    //m_confCollectDistance = ConfigMgr::GetIntDefault("Bot.Collect.Distance", 25);
    //if (m_confCollectDistance > m_confCollectDistanceMax)
    //{
    //    //sLog.outError("Playerbot: PlayerbotAI.Collect.Distance higher than PlayerbotAI.Collect.DistanceMax. Using DistanceMax value");
    //    m_confCollectDistance = m_confCollectDistanceMax;
    //}
}

PlayerbotMgrOwner::~PlayerbotMgrOwner()
{
    ASSERT(_playerBotsOwner.empty());
}

void PlayerbotMgrOwner::LogoutAllBots()
{
    RemoveAllBotsFromGroup();
    while (true)
    {
        PlayerBotMap::const_iterator itr = GetPlayerBotsBegin();
        if (itr == GetPlayerBotsEnd()) break;
        Player* bot = itr->second;
        LogoutPlayerBot(bot->GetGUID());
    }
}

// Playerbot mod: logs out a Playerbot.
void PlayerbotMgrOwner::LogoutPlayerBot(uint64 guid, bool uninvite)
{
    if (Player* bot = GetPlayerBot(guid))
    {
        if (uninvite)
            if (Group* group = bot->GetGroup())
                group->RemoveMember(guid);

        WorldSession* botWorldSession = bot->GetSession();
        _playerBotsOwner.erase(guid);               //deletes bot player ptr inside this PlayerBotMap
        botWorldSession->LogoutPlayer(true);    //this will delete the bot Player object and PlayerbotAI (along with class AI) object
        botWorldSession->m_master = NULL;       //unset bot global indentifier
        delete botWorldSession;                 //finally delete the bot's WorldSession
        botWorldSession = NULL;
        return;
    }
    std::string name;
    sObjectMgr->GetPlayerNameByGUID(guid, name);
    sLog->outFatal(LOG_FILTER_PLAYER, "PlayerbotMgrOwner:: Trying to logout playerbot (%s, guid %u) which does not exist!!!", name.c_str(), GUID_LOPART(guid));
}

// Playerbot mod: Gets a player bot Player object for this WorldSession master
Player* PlayerbotMgrOwner::GetPlayerBot(uint64 playerGuid) const
{
    PlayerBotMap::const_iterator it = _playerBotsOwner.find(playerGuid);
    return (it == _playerBotsOwner.end()) ? NULL : it->second;
}

void PlayerbotMgrOwner::OnBotLogin(Player* const bot)
{
    if (!bot)
        return;

    // tell the world session that they now manage this new bot
    _playerBotsOwner[bot->GetGUID()] = bot;

    // if bot is in a group and master is not in group then
    // have bot leave their group
    if (bot->GetGroup() && (!m_master->GetGroup() || !m_master->GetGroup()->IsMember(bot->GetGUID())))
        bot->RemoveFromGroup();

    // sometimes master can lose leadership, pass leadership to master check
    uint64 masterGuid = m_master->GetGUID();
    if (m_master->GetGroup() && !m_master->GetGroup()->IsLeader(masterGuid))
    {
        // But only do so if one of the master's bots is leader
        for (PlayerBotMap::const_iterator itr = GetPlayerBotsBegin(); itr != GetPlayerBotsEnd(); itr++)
        {
            Player* bot = itr->second;
            if (m_master->GetGroup()->IsLeader(bot->GetGUID()))
            {
                m_master->GetGroup()->ChangeLeader(masterGuid);
                break;
            }
        }
    }
    // Finally, give the bot some AI, object is owned by the player class
    PlayerbotAI* ai = new PlayerbotAI(this, bot);
    bot->SetPlayerbotAI(ai);
}

void PlayerbotMgrOwner::RemoveAllBotsFromGroup(bool force)
{
    Group* gr = m_master->GetGroup();
    if (gr || force)
        for (PlayerBotMap::const_iterator it = GetPlayerBotsBegin(); m_master->GetGroup() && it != GetPlayerBotsEnd(); ++it)
            if (force || gr == it->second->GetGroup())
                gr->RemoveMember(it->first);
}

void Player::GetSkills(std::list<uint32>& m_spellsToLearn)
{
    for (SkillStatusMap::const_iterator itr = mSkillStatus.begin(); itr != mSkillStatus.end(); ++itr)
    {
        if (itr->second.uState == SKILL_DELETED)
            continue;

        m_spellsToLearn.push_back(itr->first);
    }
}

void Player::MakeTalentGlyphLink(std::ostringstream &out)
{
    // |cff4e96f7|Htalent:1396:4|h[Unleashed Fury]|h|r
    // |cff66bbff|Hglyph:23:460|h[Glyph of Fortitude]|h|r

    if (m_specsCount)
        // loop through all specs (only 1 for now)
        for (uint32 specIdx = 0; specIdx < m_specsCount; ++specIdx)
        {
            // find class talent tabs (all players have 3 talent tabs)
            uint32 const* talentTabIds = GetTalentTabPages(getClass());

            out << "\n" << "Active Talents ";

            for (uint32 i = 0; i < 3; ++i)
            {
                uint32 talentTabId = talentTabIds[i];
                for (PlayerTalentMap::iterator iter = m_talents[specIdx]->begin(); iter != m_talents[specIdx]->end(); ++iter)
                {
                    PlayerTalent *talent = (*iter).second;

                    if (talent->state == PLAYERSPELL_REMOVED)
                        continue;

                    TalentSpellPos const* talentPos = GetTalentSpellPos((*iter).first);
                    if (!talentPos)
                        continue;
                    TalentEntry const* talentInfo = sTalentStore.LookupEntry(talentPos->talent_id);
                    if (!talentInfo)
                        continue;
                    // skip another tab talents
                    if (talentInfo->TalentTab != talentTabId)
                        continue;

                    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo((*iter).first);
                    if (!spellInfo)
                        continue;

                    out << "|cff4e96f7|Htalent:" << talentInfo->TalentID << ":" << talentInfo->RankID[spellInfo->GetRank()]
                        << " |h[" << spellInfo->SpellName[GetSession()->GetSessionDbcLocale()] << "]|h|r";
                }
            }

            uint32 freepoints = 0;

            out << " Unspent points : ";

            if ((freepoints = GetFreeTalentPoints()) > 0)
                out << "|h|cff00ff00" << freepoints << "|h|r";
            else
                out << "|h|cffff0000" << freepoints << "|h|r";

            out << "\n" << "Active Glyphs ";
            // GlyphProperties.dbc
            for (uint8 i = 0; i < MAX_GLYPH_SLOT_INDEX; ++i)
            {
                GlyphPropertiesEntry const* glyph = sGlyphPropertiesStore.LookupEntry(m_Glyphs[specIdx][i]);
                if (!glyph)
                    continue;

                SpellEntry const* spell_entry = sSpellStore.LookupEntry(glyph->SpellId);

                out << "|cff66bbff|Hglyph:" << GetGlyphSlot(i) << ":" << m_Glyphs[specIdx][i]
                    << " |h[" << spell_entry->SpellName[GetSession()->GetSessionDbcLocale()] << "]|h|r";

            }
        }
}

void Player::chompAndTrim(std::string& str)
{
    while (str.length() > 0)
    {
        char lc = str[str.length() - 1];
        if (lc == '\r' || lc == '\n' || lc == ' ' || lc == '"' || lc == '\'')
            str = str.substr(0, str.length() - 1);
        else
            break;
    }

    while (str.length() > 0)
    {
        char lc = str[0];
        if (lc == ' ' || lc == '"' || lc == '\'')
            str = str.substr(1, str.length() - 1);
        else
            break;
    }
}

bool Player::getNextQuestId(std::string const& pString, uint32& pStartPos, uint32& pId)
{
    bool result = false;
    uint32 i;
    for (i = pStartPos; i < pString.size(); ++i)
    {
        if (pString[i] == ',')
            break;
    }
    if (i > pStartPos)
    {
        std::string idString = pString.substr(pStartPos, i - pStartPos);
        pStartPos = i + 1;
        chompAndTrim(idString);
        pId = atoi(idString.c_str());
        result = true;
    }
    return result;
}

void Player::UpdateMail()
{
    // save money,items and mail to prevent cheating
    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    SaveGoldToDB(trans);
    SaveInventoryAndGoldToDB(trans);
    _SaveMail(trans);
    CharacterDatabase.CommitTransaction(trans);
}

uint32 PlayerbotMgrOwner::GetSpec(Player* player)
{
    uint32 row = 0;
    uint32 spec = 0;
    uint8 rank = 0;
    uint32 m_activeSpec = player->GetActiveSpec();
    PlayerTalentMap* m_talents = player->GetTalents(m_activeSpec);

    for (PlayerTalentMap::iterator iter = m_talents->begin(); iter != m_talents->end(); ++iter)
    {
        PlayerTalent* talent = (*iter).second;
        if (talent->state == PLAYERSPELL_REMOVED)
            continue;
        TalentSpellPos const* talentPos = GetTalentSpellPos((*iter).first);
        if (!talentPos)
            continue;
        TalentEntry const* talentInfo = sTalentStore.LookupEntry(talentPos->talent_id);
        if (!talentInfo)
            continue;

        //spec is set with 3 rules
        //a) there is no spec set yet
        //b) found talent deeper in talent tree
        //c) found talent with same depth, but with higher rank
        if (spec == 0 || talentInfo->Row > row || (talentInfo->Row == row && talentPos->rank > rank))
        {
            row = talentInfo->Row;
            spec = talentInfo->TalentTab;
        }
    }

    //Return the tree with the deepest talent
    return spec;
}

bool Player::HavePBot() const
{
    return _playerbotMgrOwner != NULL && !_playerbotMgrOwner->getPlayerbots().empty();
}

uint16 PlayerbotMgrOwner::Rand() const
{
    return urand(0, 100 + _playerBotsOwner.empty() ? 0 : (_playerBotsOwner.size() - 1) * 10);
}

PlayerbotMgrAcceptor::PlayerbotMgrAcceptor(Player* const master) : _acceptor(master) { }

PlayerbotMgrAcceptor::~PlayerbotMgrAcceptor()
{
    ASSERT(_playerBotsAcceptor.empty());
}

Player* PlayerbotMgrAcceptor::GetPlayerBot(uint64 guid) const
{
    if (!_playerBotsAcceptor.empty())
    {
        PlayerBotMap::const_iterator it;
        for (AcceptedBots::const_iterator itr = _playerBotsAcceptor.begin(); itr != _playerBotsAcceptor.end(); ++itr)
        {
            it = itr->second->find(guid);
            if (it != itr->second->end())
                return it->second;
        }
    }
    return NULL;
}

void PlayerbotMgrAcceptor::AcceptPlayerBot(Player* bot)
{
    uint64 owGuid = bot->GetPlayerbotAI()->GetOwnerGuid();
    PlayerBotMap* map;
    AcceptedBots::iterator itr = _playerBotsAcceptor.find(owGuid);
    if (itr != _playerBotsAcceptor.end())
    {
        map = _playerBotsAcceptor[owGuid];
        (*map)[bot->GetGUID()] = bot;
    }
    else
    {
        map = new PlayerBotMap();
        (*map)[bot->GetGUID()] = bot;
        _playerBotsAcceptor[owGuid] = map;
    }
}

void PlayerbotMgrAcceptor::RemoveBotFromMap(uint64 guid)
{
    if (_playerBotsAcceptor.empty())
        return;

    PlayerBotMap* map;
    PlayerBotMap::const_iterator it;
    for (AcceptedBots::const_iterator itr = _playerBotsAcceptor.begin(); itr != _playerBotsAcceptor.end(); ++itr)
    {
        map = itr->second;
        it = map->find(guid);
        if (it != map->end())
        {
            map->erase(guid);
            if (map->empty())
            {
                _playerBotsAcceptor.erase(itr->first);
                delete map;
                map = NULL;
            }

            break;
        }
    }
}

void PlayerbotMgrAcceptor::RemoveBotFromGroup(uint64 guid)
{
    if (_playerBotsAcceptor.empty())
        return;

    Group* gr = _acceptor->GetGroup();
    if (!gr)
        return;

    for (AcceptedBots::const_iterator itr = _playerBotsAcceptor.begin(); _acceptor->GetGroup() && itr != _playerBotsAcceptor.end(); ++itr)
        for (PlayerBotMap::const_iterator it = itr->second->begin(); _acceptor->GetGroup() && it != itr->second->end(); ++it)
            if (gr == it->second->GetGroup())
                if (guid == 0 || guid == it->second->GetGUID())
                    gr->RemoveMember(it->first);
}

void PlayerbotMgrAcceptor::RemoveAllBotsOnLogout()
{
    AcceptedBots::iterator itr;
    PlayerBotMap::iterator it;
    PlayerBotMap* map;
    while (!_playerBotsAcceptor.empty())
    {
        itr = _playerBotsAcceptor.begin();

        map = itr->second;
        while (!map->empty())
        {
            it = map->begin();
            map->erase(it->first);
        }

        _playerBotsAcceptor.erase(itr->first);
        delete map;
        map = NULL;
    }
}

bool Player::HaveAccPBot() const
{
    return _playerbotMgrAcceptor != NULL && !_playerbotMgrAcceptor->getAcceptedPlayerbots().empty();
}
