/*
Playerbot Helper by Graff (onlysuffering@gmail.com)
Type: empty-type player-to-self dialog-like manager for playerbot mod
Complete: ???
Category: scripts/custom/playerbots
*/

#include "bp_ai.h"
#include "bp_hlp.h"
#include "bp_mgr.h"
#include "Chat.h"
#include "Config.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Group.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"

const uint8 GroupIcons[TARGETICONCOUNT] =
{
    /*STAR        = */0x001,
    /*CIRCLE      = */0x002,
    /*DIAMOND     = */0x004,
    /*TRIANGLE    = */0x008,
    /*MOON        = */0x010,
    /*SQUARE      = */0x020,
    /*CROSS       = */0x040,
    /*SKULL       = */0x080
};

enum HelperActions
{
    ACTION_ENABLE                                               = 1,
    ACTION_DISABLE                                              = 2,
    ACTION_TOGGLE                                               = 3
};

enum BotgiverTexIDs
{
    ABANDON_PLAYER                                              = 1,
    RECRUIT_PLAYER                                              = 2,
    ABANDON_MINION                                              = 3,
    RECRUIT_MINION                                              = 4,
    ABOUT_STR                                                   = 5,
    ADD_ALL                                                     = 6,
    REMOVE_ALL                                                  = 7,
    RECRUIT_WARRIOR                                             = 8,
    RECRUIT_HUNTER                                              = 9,
    RECRUIT_PALADIN                                             = 10,
    RECRUIT_SHAMAN                                              = 11,
    RECRUIT_ROGUE                                               = 12,
    RECRUIT_DRUID                                               = 13,
    RECRUIT_MAGE                                                = 14,
    RECRUIT_PRIEST                                              = 15,
    RECRUIT_WARLOCK                                             = 16,
    RECRUIT_DEATH_KNIGHT                                        = 17,
    ABOUT_BASIC_STR1                                            = 18,
    ABOUT_BASIC_STR2                                            = 19,
    ABOUT_BASIC_STR3                                            = 20,
    ABOUT_ICONS_STR1                                            = 21,
    ABOUT_ICONS_STR2                                            = 22,
    ICON_STRING_STAR                                            = 23,
    ICON_STRING_CIRCLE                                          = 24,
    ICON_STRING_DIAMOND                                         = 25,
    ICON_STRING_TRIANGLE                                        = 26,
    ICON_STRING_MOON                                            = 27,
    ICON_STRING_SQUARE                                          = 28,
    ICON_STRING_CROSS                                           = 29,
    ICON_STRING_SKULL                                           = 30,
    ICON_STRING_UNKNOWN                                         = 31,
    NO_MORE_AVAILABLE                                           = 32,
    ONE_MORE_AVAILABLE                                          = 33,
    SOME_MORE_AVAILABLE                                         = 34,
    ONE_AVAILABLE                                               = 35,
    SOME_AVAILABLE                                              = 36,
    MANAGE_BOTS                                                 = 37,
    MANAGE_BOT_SET_FOLLOW                                       = 38,
    MANAGE_BOT_SET_FOLLOW_SELECTION                             = 39,
    MANAGE_BOT_SET_FOLLOW_NAME                                  = 40,
    MANAGE_BOT_SET_FREEROAM                                     = 41,


    ENABLE_STRING,
    DISABLE_STRING,
    TOGGLE_STRING,
    BACK_STRING,
    ALL_STRING,
    MAX_STRINGS
};

enum HelperGossip
{
    SENDER_CREATE_NBOT_MENU                                     = 1,
    SENDER_CREATE_NBOT                                          = 2,
    SENDER_CREATE_PBOT_MENU                                     = 3,
    SENDER_CREATE_PBOT                                          = 4,

    SENDER_REMOVE_PBOT_MENU                                     = 5,
    SENDER_REMOVE_PBOT                                          = 6,
    SENDER_REMOVE_NBOT_MENU                                     = 7,
    SENDER_REMOVE_NBOT                                          = 8,

    SENDER_INFO_WHISPER                                         = 9,

    SENDER_MANAGE_BOTS                                          = 10,
    SENDER_MANAGE_BOTS_SET_FOLLOW                               = 11,
    SENDER_MANAGE_BOTS_SET_FOLLOW_WHO                           = 12,
    SENDER_MANAGE_BOTS_SET_FOLLOW_SELECTION                     = 13,
    SENDER_MANAGE_BOTS_SET_FOLLOW_NAME                          = 14,

    SENDER_MANAGE_BOTS_SET_FREEROAM                             = 15,
    SENDER_MANAGE_BOTS_SET_FREEROAM_SET                         = 16,


    SENDER_SELECT_BOT,
    SENDER_MAIN_PAGE,
    MAX_SENDERS
};

typedef std::list<uint64> BotsList;
BotsList _bots;
PlayerBotMap _listedBots;

PlayerbotHelper::PlayerbotHelper(Player* const master) : _master(master) { }
PlayerbotHelper::~PlayerbotHelper() {}

bool PlayerbotHelper::OnGossipSelect(Player* player, uint32 sender, uint32 action)
{
    switch (sender)
    {
        case SENDER_MAIN_PAGE:                          OnGossipHello(player);                              break;
        case SENDER_SELECT_BOT:                         SendBotSelectionMenu(player, action);               break;

        case SENDER_CREATE_NBOT_MENU:                   SendCreateNPCBotMenu(player, action);               break;
        case SENDER_CREATE_NBOT:                        SendCreateNPCBot(player, action);                   break;
        case SENDER_CREATE_PBOT_MENU:                   SendCreatePlayerBotMenu(player, action);            break;
        case SENDER_CREATE_PBOT:                        SendCreatePlayerBot(player, action);                break;

        case SENDER_REMOVE_PBOT_MENU:                   SendRemovePlayerBotMenu(player, action);            break;
        case SENDER_REMOVE_PBOT:                        SendRemovePlayerBot(player, action);                break;
        case SENDER_REMOVE_NBOT_MENU:                   SendRemoveNPCBotMenu(player, action);               break;
        case SENDER_REMOVE_NBOT:                        SendRemoveNPCBot(player, action);                   break;

        case SENDER_INFO_WHISPER:                       SendBotHelpWhisper(player, action);                 break;

        case SENDER_MANAGE_BOTS:                        SendManageBotsMenu(player, action);                 break;
        case SENDER_MANAGE_BOTS_SET_FOLLOW:             SendManageBotsFollowMenu(player, action);           break;
        case SENDER_MANAGE_BOTS_SET_FOLLOW_SELECTION:   SendManageBotsFollowSelection(player, action);      break;
        case SENDER_MANAGE_BOTS_SET_FOLLOW_NAME:        SendManageBotsFollowName(player, action);           break;

        case SENDER_MANAGE_BOTS_SET_FREEROAM:           SendManageBotsFreeRoamMenu(player, action);         break;
        case SENDER_MANAGE_BOTS_SET_FREEROAM_SET:       SendManageBotsFreeRoamSet(player, action);          break;
        default:
            break;
    }
    return true;
}
//SENDER_SELECT_BOT
//sends available bots list with 'senderAction' assigned as new sender
//senderAction must be 'original sender + GOSSIP_ACTION_INFO_DEF' to prevent unexpected behaviour
void PlayerbotHelper::SendBotSelectionMenu(Player* player, uint32 senderAction)
{
    _listedBots.clear();
    player->PlayerTalkClass->ClearMenus();
    PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner();
    if (!mgr)
    {
        //sLog
        player->CLOSE_GOSSIP_MENU();
        return;
    }
    senderAction -= GOSSIP_ACTION_INFO_DEF;

    uint8 x = 1;
    if (mgr->GetPlayerBotsCount() != 1)
    {
        std::string tempstr = "ALL";
        player->PlayerTalkClass->GetGossipMenu().AddMenuItem(x, 9, GetLocaleStringForTextID(tempstr, ALL_STRING, player->GetSession()->GetSessionDbLocaleIndex()), senderAction, GOSSIP_ACTION_INFO_DEF + x, "", 0);
    }

    ++x;
    for (PlayerBotMap::const_iterator itr = mgr->GetPlayerBotsBegin(); itr != mgr->GetPlayerBotsEnd(); ++itr)
    {
        _listedBots[x] = itr->second;
        player->PlayerTalkClass->GetGossipMenu().AddMenuItem(x, 9, itr->second->GetName(), senderAction, GOSSIP_ACTION_INFO_DEF + x, "", 0);
        ++x;
    }

    std::string tempstr = "BACK";
    player->PlayerTalkClass->GetGossipMenu().AddMenuItem(11, 0, GetLocaleStringForTextID(tempstr, BACK_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MAIN_PAGE, GOSSIP_ACTION_INFO_DEF + 1000, "", 0);
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}

bool PlayerbotHelper::OnGossipHello(Player* player)
{
    player->PlayerTalkClass->ClearMenus(); //in case of return;

    uint8 count = 0;

    uint8 maxPBcount = ConfigMgr::GetIntDefault("Bot.MaxPlayerbots", 9);
    uint8 maxNBcount = player->GetMaxNpcBots();

    bool allowPBots = ConfigMgr::GetBoolDefault("Bot.EnablePlayerBots", false);
    bool allowNBots = ConfigMgr::GetBoolDefault("Bot.EnableNpcBots", true) && !player->RestrictBots();

    std::string tempstr;

    if (PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner())
    {
        for (PlayerBotMap::const_iterator itr = mgr->GetPlayerBotsBegin(); itr != mgr->GetPlayerBotsEnd(); ++itr)
        {
            if (count == 0)
            {
                tempstr = "Abandon my Player";
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(3, 0, GetLocaleStringForTextID(tempstr, ABANDON_PLAYER, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_REMOVE_PBOT_MENU, GOSSIP_ACTION_INFO_DEF + 3, "", 0);
            }
            ++count;
        }
        if (count != 0)
        {
            tempstr = "Manage Bots";
            player->PlayerTalkClass->GetGossipMenu().AddMenuItem(5, 0, GetLocaleStringForTextID(tempstr, MANAGE_BOTS, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_SELECT_BOT, GOSSIP_ACTION_INFO_DEF + SENDER_MANAGE_BOTS, "", 0);
        }
    }
    if (count < maxPBcount && allowPBots)
    {
        tempstr = "Recruit a Player";
        player->PlayerTalkClass->GetGossipMenu().AddMenuItem(1, 0, GetLocaleStringForTextID(tempstr, RECRUIT_PLAYER, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_CREATE_PBOT_MENU, GOSSIP_ACTION_INFO_DEF + 1, "", 0);
    }

    if (player->HaveBot())
    {
        count = player->GetNpcBotsCount();
        if (count > 0)
        {
            tempstr = "Abandon my Minion";
            player->PlayerTalkClass->GetGossipMenu().AddMenuItem(4, 0, GetLocaleStringForTextID(tempstr, ABANDON_MINION, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_REMOVE_NBOT_MENU, GOSSIP_ACTION_INFO_DEF + 4, "", 0);
        }
        if (count < maxNBcount && allowNBots)
        {
            tempstr = "Recruit a Minion";
            player->PlayerTalkClass->GetGossipMenu().AddMenuItem(2, 0, GetLocaleStringForTextID(tempstr, RECRUIT_MINION, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_CREATE_NBOT_MENU, GOSSIP_ACTION_INFO_DEF + 2, "", 0);
        }
    }
    else if (allowNBots && maxNBcount != 0)
    {
        tempstr = "Recruit a Minion";
        player->PlayerTalkClass->GetGossipMenu().AddMenuItem(2, 0, GetLocaleStringForTextID(tempstr, RECRUIT_MINION, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_CREATE_NBOT_MENU, GOSSIP_ACTION_INFO_DEF + 2, "", 0);
    }

    tempstr = "Tell me about these bots";
    player->PlayerTalkClass->GetGossipMenu().AddMenuItem(6, 0, GetLocaleStringForTextID(tempstr, ABOUT_STR, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_INFO_WHISPER, GOSSIP_ACTION_INFO_DEF + 6, "", 0);

    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
    return true;
}

void PlayerbotHelper::SendCreatePlayerBot(Player* player, uint32 action)
{
    std::string plName;
    std::list<std::string>* names = new std::list<std::string>;
    uint32 accId = player->GetSession()->GetAccountId();
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PLAYERBOTS);
    stmt->setUInt32(0, accId);
    stmt->setUInt32(1, player->GetGUIDLow());
    PreparedQueryResult results = CharacterDatabase.Query(stmt);
    //QueryResult results = CharacterDatabase.PQuery("SELECT name FROM characters WHERE account = '%u' AND guid != '%u'", accId, player->GetGUIDLow());
    if (results)
    {
        do
        {
            Field* fields = results->Fetch();
            plName = fields[0].GetString();
            if (ObjectAccessor::FindPlayerByName(plName))
                continue;
            names->insert(names->end(), plName);
        } while (results->NextRow());
    }

    if (names->empty())
    {
        delete names;
        player->CLOSE_GOSSIP_MENU();
        return;
    }

    PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner();
    if (!mgr)
    {
        mgr = new PlayerbotMgrOwner(player);
        player->SetPlayerbotMgrOwner(mgr);
    }

    int8 x = action - GOSSIP_ACTION_INFO_DEF - 1;

    for (std::list<std::string>::iterator iter = names->begin(); iter != names->end(); iter++)
    {
        if (x == 0)
        {
            uint64 guid = sObjectMgr->GetPlayerGUIDByName((*iter).c_str());
            if (mgr->GetPlayerBot(guid) != NULL)
                continue;
            mgr->AddPlayerBot(guid);
        }
        else
        {
            if (x == 1)
            {
                uint64 guid = sObjectMgr->GetPlayerGUIDByName((*iter).c_str());
                if (mgr->GetPlayerBot(guid) != NULL)
                    break;
                mgr->AddPlayerBot(guid);
                break;
            }
            --x;
        }
    }
    player->CLOSE_GOSSIP_MENU();
    delete names;
}

void PlayerbotHelper::SendCreatePlayerBotMenu(Player* player, uint32 /*action*/)
{
    std::string plName;
    std::list<std::string>* names = new std::list<std::string>;
    uint32 accId = player->GetSession()->GetAccountId();
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PLAYERBOTS);
    stmt->setUInt32(0, accId);
    stmt->setUInt32(1, player->GetGUIDLow());
    PreparedQueryResult results = CharacterDatabase.Query(stmt);
    //QueryResult results = CharacterDatabase.PQuery("SELECT name FROM characters WHERE account = '%u' AND guid != '%u'", accId, player->GetGUIDLow());
    if (results)
    {
        do
        {
            Field* fields = results->Fetch();
            plName = fields[0].GetString();
            if (ObjectAccessor::FindPlayerByName(plName))
                continue;
            names->insert(names->end(), plName);
        } while (results->NextRow());
    }

    if (names->empty())
    {
        delete names;
        player->CLOSE_GOSSIP_MENU();
        return;
    }

    player->PlayerTalkClass->ClearMenus();
    std::string tempstr;
    uint8 maxPBcount = ConfigMgr::GetIntDefault("Bot.MaxPlayerbots", 9);
    PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner();
    uint8 bots = !mgr ? 0 : mgr->GetPlayerBotsCount();
    uint32 freePBSlots = maxPBcount - bots;
    if (freePBSlots != 1 && freePBSlots >= names->size())
    {
        tempstr = "ADD ALL";
        player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, ADD_ALL, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_CREATE_PBOT, GOSSIP_ACTION_INFO_DEF + 1);
    }

    int8 x = 2;
    for (std::list<std::string>::iterator iter = names->begin(); iter != names->end(); iter++)
    {
        player->ADD_GOSSIP_ITEM(9, (*iter).c_str() , SENDER_CREATE_PBOT, GOSSIP_ACTION_INFO_DEF + x);
        ++x;
    }

    std::ostringstream buff;
    if (freePBSlots == 0)
    {
        tempstr = "no more bots available";
        buff << GetLocaleStringForTextID(tempstr, NO_MORE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
    }
    else
    {
        buff << freePBSlots;
        buff << ' ';
        if (freePBSlots == 1)
        {
            if (bots == 0)
            {
                tempstr = "bot available";
                buff << GetLocaleStringForTextID(tempstr, ONE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
            else
            {
                tempstr = "more bot available";
                buff << GetLocaleStringForTextID(tempstr, ONE_MORE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
        }
        else
        {
            if (bots == 0)
            {
                tempstr = "bots available";
                buff << GetLocaleStringForTextID(tempstr, SOME_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
            else
            {
                tempstr = "more bots available";
                buff << GetLocaleStringForTextID(tempstr, SOME_MORE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
        }
    }

    player->ADD_GOSSIP_ITEM(0, buff.str(), SENDER_CREATE_PBOT_MENU, GOSSIP_ACTION_INFO_DEF + 1);

    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());

    delete names;
} //end SendCreatePlayerBotMenu

void PlayerbotHelper::SendRemovePlayerBotAll(Player* player)
{
    for (int8 x = 2; x<=10; x++ )
        SendRemovePlayerBot(player, GOSSIP_ACTION_INFO_DEF + 2);
}

void PlayerbotHelper::SendRemovePlayerBot(Player* player, uint32 action)
{
    int8 x = action - GOSSIP_ACTION_INFO_DEF - 1;

    if (x == 0)
    {
        SendRemovePlayerBotAll(player);
        return;
    }

    if (PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner())
    {
        for (PlayerBotMap::const_iterator itr = mgr->GetPlayerBotsBegin(); itr != mgr->GetPlayerBotsEnd(); ++itr)
        {
            if (x == 1 && itr->second)
            {
                mgr->LogoutPlayerBot(itr->second->GetGUID());
                break;
            }
            --x;
        }
    }
    player->CLOSE_GOSSIP_MENU();
} //end SendRemovePlayerBot

void PlayerbotHelper::SendRemovePlayerBotMenu(Player* player, uint32 /*action*/)
{
    player->PlayerTalkClass->ClearMenus();
    PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner();
    if (mgr->GetPlayerBotsCount() != 1)
    {
        std::string tempstr = "REMOVE ALL";
        player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, REMOVE_ALL, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_REMOVE_PBOT, GOSSIP_ACTION_INFO_DEF + 1);
    }

    uint8 x = 2;
    for (PlayerBotMap::const_iterator itr = mgr->GetPlayerBotsBegin(); itr != mgr->GetPlayerBotsEnd(); ++itr)
    {
        Player* bot = itr->second;
        player->ADD_GOSSIP_ITEM(9, bot->GetName(), SENDER_REMOVE_PBOT, GOSSIP_ACTION_INFO_DEF + x);
        ++x;
    }
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
} //end SendRemovePlayerBotMenu

void PlayerbotHelper::SendRemoveNPCBot(Player* player, uint32 action)
{
    int8 x = action - GOSSIP_ACTION_INFO_DEF;
    if (x == 1)
    {
        player->CLOSE_GOSSIP_MENU();
        for (uint8 i = 0; i != player->GetMaxNpcBots(); ++i)
            player->RemoveBot(player->GetBotMap(i)->_Guid(), true);
        return;
    }
    for (uint8 i = 0; i != player->GetMaxNpcBots(); ++i)
    {
        if (!player->GetBotMap(i)->_Cre())
            continue;
        if (x == 2)
        {
            player->RemoveBot(player->GetBotMap(i)->_Guid(), true);
            break;
        }
        --x;
    }
    player->CLOSE_GOSSIP_MENU();
}

void PlayerbotHelper::SendRemoveNPCBotMenu(Player* player, uint32 /*action*/)
{
    player->PlayerTalkClass->ClearMenus();
    if (player->GetNpcBotsCount() == 1)
    {
        for (uint8 i = 0; i != player->GetMaxNpcBots(); ++i)
            player->RemoveBot(player->GetBotMap(i)->_Guid(), true);
        player->CLOSE_GOSSIP_MENU();
        return;
    }
    std::string tempstr = "REMOVE ALL";
    player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, REMOVE_ALL, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_REMOVE_NBOT, GOSSIP_ACTION_INFO_DEF + 1);

    uint8 x = 2;
    for (uint8 i = 0; i != player->GetMaxNpcBots(); ++i)
    {
        Creature* bot = player->GetBotMap(i)->_Cre();
        if (!bot) continue;
        player->ADD_GOSSIP_ITEM(9, bot->GetName(), SENDER_REMOVE_NBOT, GOSSIP_ACTION_INFO_DEF + x);
        ++x;
    }
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}

void PlayerbotHelper::SendCreateNPCBot(Player* player, uint32 action)
{
    uint8 bot_class = 0;
    if (action == GOSSIP_ACTION_INFO_DEF + 1)//"Back"
    {
        player->CLOSE_GOSSIP_MENU();
        return;
    }
    else if (action == GOSSIP_ACTION_INFO_DEF + 2)
        bot_class = CLASS_WARRIOR;
    //else if (action == GOSSIP_ACTION_INFO_DEF + 3)
    //    bot_class = CLASS_HUNTER;
    else if (action == GOSSIP_ACTION_INFO_DEF + 4)
        bot_class = CLASS_PALADIN;
    //else if (action == GOSSIP_ACTION_INFO_DEF + 5)
    //    bot_class = CLASS_SHAMAN;
    else if (action == GOSSIP_ACTION_INFO_DEF + 6)
        bot_class = CLASS_ROGUE;
    else if (action == GOSSIP_ACTION_INFO_DEF + 7)
        bot_class = CLASS_DRUID;
    else if (action == GOSSIP_ACTION_INFO_DEF + 8)
        bot_class = CLASS_MAGE;
    else if (action == GOSSIP_ACTION_INFO_DEF + 9)
        bot_class = CLASS_PRIEST;
    else if (action == GOSSIP_ACTION_INFO_DEF + 10)
        bot_class = CLASS_WARLOCK;
    //else if (action == GOSSIP_ACTION_INFO_DEF + 11)
    //    bot_class = CLASS_DEATH_KNIGHT;

    if (bot_class != 0)
        player->CreateNPCBot(bot_class);
    player->CLOSE_GOSSIP_MENU();
    return;
}

void PlayerbotHelper::SendCreateNPCBotMenu(Player* player, uint32 /*action*/)
{
    std::string cost = player->GetNpcBotCostStr();
    player->PlayerTalkClass->ClearMenus();

    std::string tempstr = "Recruit a Warrior ";
    player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, RECRUIT_WARRIOR, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 2);
    //tempstr = "Recruit a Hunter ";
    //player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, RECRUIT_HUNTER, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 3);
    tempstr = "Recruit a Paladin ";
    player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, RECRUIT_PALADIN, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 4);
    //tempstr = "Recruit a Shaman ";
    //player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, RECRUIT_SHAMAN, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 5);
    tempstr = "Recruit a Rogue ";
    player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, RECRUIT_ROGUE, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 6);
    tempstr = "Recruit a Druid ";
    player->ADD_GOSSIP_ITEM(3, GetLocaleStringForTextID(tempstr, RECRUIT_DRUID, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 7);
    tempstr = "Recruit a Mage ";
    player->ADD_GOSSIP_ITEM(3, GetLocaleStringForTextID(tempstr, RECRUIT_MAGE, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 8);
    tempstr = "Recruit a Priest ";
    player->ADD_GOSSIP_ITEM(3, GetLocaleStringForTextID(tempstr, RECRUIT_PRIEST, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 9);
    tempstr = "Recruit a Warlock ";
    player->ADD_GOSSIP_ITEM(3, GetLocaleStringForTextID(tempstr, RECRUIT_WARLOCK, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 10);
    //tempstr = "Recruit a Death Knight ";
    //player->ADD_GOSSIP_ITEM(9, GetLocaleStringForTextID(tempstr, RECRUIT_DEATH_KNIGHT, player->GetSession()->GetSessionDbLocaleIndex()) + cost, SENDER_CREATE_NBOT, GOSSIP_ACTION_INFO_DEF + 11);

    std::ostringstream buff;
    uint8 bots = player->GetNpcBotsCount();
    uint8 maxNBcount = player->GetMaxNpcBots();
    uint32 freeNBSlots = maxNBcount - bots;

    if (freeNBSlots == 0)
    {
        tempstr = "no more bots available";
        buff << GetLocaleStringForTextID(tempstr, NO_MORE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
    }
    else
    {
        buff << freeNBSlots;
        buff << ' ';
        if (freeNBSlots == 1)
        {
            if (bots == 0)
            {
                tempstr = "bot available";
                buff << GetLocaleStringForTextID(tempstr, ONE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
            else
            {
                tempstr = "more bot available";
                buff << GetLocaleStringForTextID(tempstr, ONE_MORE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
        }
        else
        {
            if (bots == 0)
            {
                tempstr = "bots available";
                buff << GetLocaleStringForTextID(tempstr, SOME_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
            else
            {
                tempstr = "more bots available";
                buff << GetLocaleStringForTextID(tempstr, SOME_MORE_AVAILABLE, player->GetSession()->GetSessionDbLocaleIndex());
            }
        }
    }
    player->ADD_GOSSIP_ITEM(0, buff.str(), SENDER_CREATE_NBOT_MENU, GOSSIP_ACTION_INFO_DEF + 2);

    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}
//SENDER_MANAGE_BOTS
void PlayerbotHelper::SendManageBotsMenu(Player* player, uint32 action)
{
    player->PlayerTalkClass->ClearMenus();
    std::string tempstr = "Set follow target";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, MANAGE_BOT_SET_FOLLOW, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FOLLOW, action);
    tempstr = "Toggle free roam mode";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, MANAGE_BOT_SET_FREEROAM, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FREEROAM, action);
    tempstr = "BACK";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, BACK_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MAIN_PAGE, GOSSIP_ACTION_INFO_DEF + 1000);
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}
//SENDER_MANAGE_BOTS_SET_FOLLOW
void PlayerbotHelper::SendManageBotsFollowMenu(Player* player, uint32 action)
{
    CreateBotList(player, action);

    std::string tempstr = "Give name";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, MANAGE_BOT_SET_FOLLOW_NAME, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FOLLOW_NAME, GOSSIP_ACTION_INFO_DEF + 1);
    tempstr = "Selection";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, MANAGE_BOT_SET_FOLLOW_SELECTION, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FOLLOW_SELECTION, GOSSIP_ACTION_INFO_DEF + 2);
    tempstr = "BACK";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, BACK_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MAIN_PAGE/*SENDER_MANAGE_BOTS*/, GOSSIP_ACTION_INFO_DEF + 1000);
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}
//SENDER_MANAGE_BOTS_SET_FOLLOW_NAME
void PlayerbotHelper::SendManageBotsFollowName(Player* player, uint32 /*action*/)
{
    player->PlayerTalkClass->ClearMenus();
    player->ADD_GOSSIP_ITEM(0, "Sorry NYI", SENDER_MANAGE_BOTS_SET_FOLLOW, GOSSIP_ACTION_INFO_DEF + 1);
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}
//SENDER_MANAGE_BOTS_SET_FOLLOW_SELECTION
void PlayerbotHelper::SendManageBotsFollowSelection(Player* player, uint32 /*action*/)
{
    player->CLOSE_GOSSIP_MENU();

    uint64 selection = player->GetSelection();
    if (!selection)
    {
        ChatHandler ch(player->GetSession());
        ch.SendSysMessage(LANG_SELECT_CHAR_OR_CREATURE);
        return;
    }
    Unit* unit = ObjectAccessor::FindUnit(selection);
    if (!unit)
    {
        ChatHandler ch(player->GetSession());
        ch.SendSysMessage("Unit is not found!");
        return;
    }
    for (BotsList::const_iterator itr = _bots.begin(); itr != _bots.end(); ++itr)
    {
        Player* bot = ObjectAccessor::FindPlayer(*itr);
        if (!bot)
        {
            //sLog
            //return;
            continue;
        }
        if (!bot->IsInWorld())
        {
            //sLog
            //return
            continue;
        }
        PlayerbotAI* ai = bot->GetPlayerbotAI();
        if (selection == ai->GetOwnerGuid())
            ai->ClearBotState(BOTSTATE_INDEPENDENT);
        else
            ai->AddBotState(BOTSTATE_INDEPENDENT);

        ai->SetFollowTarget(selection);
    }

    return;
}
//SENDER_MANAGE_BOTS_SET_FREEROAM
void PlayerbotHelper::SendManageBotsFreeRoamMenu(Player* player, uint32 action)
{
    CreateBotList(player, action);

    std::string tempstr = "Enable";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, ENABLE_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FREEROAM_SET, GOSSIP_ACTION_INFO_DEF + 1);
    tempstr = "Disable";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, DISABLE_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FREEROAM_SET, GOSSIP_ACTION_INFO_DEF + 2);
    tempstr = "Toggle";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, TOGGLE_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MANAGE_BOTS_SET_FREEROAM_SET, GOSSIP_ACTION_INFO_DEF + 3);
    tempstr = "BACK";
    player->ADD_GOSSIP_ITEM(0, GetLocaleStringForTextID(tempstr, BACK_STRING, player->GetSession()->GetSessionDbLocaleIndex()), SENDER_MAIN_PAGE, GOSSIP_ACTION_INFO_DEF + 1000);
    player->PlayerTalkClass->SendGossipMenu(8446, player->GetGUID());
}
//SENDER_MANAGE_BOTS_SET_FREEROAM_SET
void PlayerbotHelper::SendManageBotsFreeRoamSet(Player* player, uint32 action)
{
    player->CLOSE_GOSSIP_MENU();
    action -= GOSSIP_ACTION_INFO_DEF;

    for (BotsList::const_iterator itr = _bots.begin(); itr != _bots.end(); ++itr)
    {
        Player* bot = ObjectAccessor::FindPlayer(*itr);
        if (!bot)
        {
            //sLog
            //return;
            continue;
        }
        if (!bot->IsInWorld())
        {
            //sLog
            //return
            continue;
        }

        PlayerbotAI* ai = bot->GetPlayerbotAI();
        if (action == ACTION_ENABLE)
        {
            ai->AddBotState(BOTSTATE_INDEPENDENT);
            ai->ClearBotState(BOTSTATE_FOLLOW);
            ai->SetFollowTarget(bot->GetGUID());
        }
        else if (action == ACTION_DISABLE)
        {
            ai->ClearBotState(BOTSTATE_INDEPENDENT);
            ai->AddBotState(BOTSTATE_FOLLOW);
            ai->SetFollowTarget(ai->GetOwnerGuid());
        }
        else //toggle
        {
            if (ai->IsInFreeRoamMode())
            {
                ai->ClearBotState(BOTSTATE_INDEPENDENT);
                ai->AddBotState(BOTSTATE_FOLLOW);
                ai->SetFollowTarget(ai->GetOwnerGuid());
            }
            else
            {
                ai->AddBotState(BOTSTATE_INDEPENDENT);
                ai->ClearBotState(BOTSTATE_FOLLOW);
                ai->SetFollowTarget(bot->GetGUID());
            }
        }
    }

    return;
}

void PlayerbotHelper::SendBotHelpWhisper(Player* player, uint32 /*action*/)
{
    player->CLOSE_GOSSIP_MENU();
    ChatHandler ch(player->GetSession());
    //Basic
    std::string tempstr = "To see list of Playerbot commands whisper 'help' to one of your playerbots";
    std::string msg1 = GetLocaleStringForTextID(tempstr, ABOUT_BASIC_STR1, player->GetSession()->GetSessionDbLocaleIndex());
    tempstr = "To see list of available npcbot commands type .npcbot or .npcb";
    std::string msg2 = GetLocaleStringForTextID(tempstr, ABOUT_BASIC_STR2, player->GetSession()->GetSessionDbLocaleIndex());
    tempstr = "You can also use .maintank (or .mt or .main) command on any party member (even npcbot) so your bots will stick to your plan";
    std::string msg3 = GetLocaleStringForTextID(tempstr, ABOUT_BASIC_STR3, player->GetSession()->GetSessionDbLocaleIndex());
    ch.SendSysMessage(msg1.c_str());
    ch.SendSysMessage(msg2.c_str());
    ch.SendSysMessage(msg3.c_str());
    //Heal Icons
    uint8 mask = ConfigMgr::GetIntDefault("Bot.HealTargetIconsMask", 8);
    std::string msg4 = "";
    if (mask == 255)
    {
        tempstr = "If you want your npcbots to heal someone out of your party set any raid target icon on them";
        msg4 = GetLocaleStringForTextID(tempstr, ABOUT_ICONS_STR1, player->GetSession()->GetSessionDbLocaleIndex());
        ch.SendSysMessage(msg4.c_str());
    }
    else if (mask != 0)
    {
        tempstr = "If you want your npcbots to heal someone out of your party set proper raid target icon on them, one of these: ";
        msg4 = GetLocaleStringForTextID(tempstr, ABOUT_ICONS_STR2, player->GetSession()->GetSessionDbLocaleIndex());
        std::string iconrow = "";
        uint8 count = 0;
        for (uint8 i = 0; i != TARGETICONCOUNT; ++i)
        {
            if (mask & GroupIcons[i])
            {
                if (count != 0)
                    iconrow += ", ";
                ++count;
                switch (i)
                {
                    case 0:
                        tempstr = "star";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_STAR, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 1:
                        tempstr = "circle";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_CIRCLE, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 2:
                        tempstr = "diamond";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_DIAMOND, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 3:
                        tempstr = "triangle";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_TRIANGLE, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 4:
                        tempstr = "moon";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_MOON, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 5:
                        tempstr = "square";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_SQUARE, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 6:
                        tempstr = "cross";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_CROSS, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    case 7:
                        tempstr = "skull";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_SKULL, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                    default:
                        tempstr = "unknown icon";
                        iconrow += GetLocaleStringForTextID(tempstr, ICON_STRING_UNKNOWN, player->GetSession()->GetSessionDbLocaleIndex());
                        break;
                }
            }
        }
        msg4 += iconrow;
        ch.SendSysMessage(msg4.c_str());
    }
}

std::string PlayerbotHelper::GetLocaleStringForTextID(std::string& textValue, uint32 textId, int32 localeIdx)
{
    if (textId >= MAX_STRINGS)
    {
        sLog->outError(LOG_FILTER_PLAYER, "botgiver:GetLocaleStringForTextID:: unknown text id: %u!", uint32(textId));
        return textValue;
    }

    if (localeIdx == DEFAULT_LOCALE)
        return textValue; //use default

    if (localeIdx < 0)
    {
        sLog->outError(LOG_FILTER_PLAYER, "botgiver:GetLocaleStringForTextID:: unknown locale: %i! Sending default locale text...", localeIdx);
        return textValue;
    }

    uint32 idxEntry = MAKE_PAIR32(60000, textId);
    if (GossipMenuItemsLocale const* no = sObjectMgr->GetGossipMenuItemsLocale(idxEntry))
        ObjectMgr::GetLocaleString(no->OptionText, localeIdx, textValue);
    return textValue;
}

void PlayerbotHelper::CreateBotList(Player* player, uint32 action)
{
    _bots.clear();
    player->PlayerTalkClass->ClearMenus();
    PlayerbotMgrOwner* mgr = player->GetPlayerbotMgrOwner();
    ASSERT(mgr);
    ASSERT(!_listedBots.empty());

    //action -= GOSSIP_ACTION_INFO_DEF;
    uint32 x = action - GOSSIP_ACTION_INFO_DEF;

    for (PlayerBotMap::const_iterator itr = _listedBots.begin(); itr != _listedBots.end(); ++itr)
        if (x == 1 || x == itr->first)
            _bots.push_back(itr->second->GetGUID());
}
