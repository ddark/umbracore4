#ifndef _PLAYERBOTMGR_H
#define _PLAYERBOTMGR_H

#include "Common.h"

class Player;

typedef UNORDERED_MAP<uint64, Player*> PlayerBotMap;
typedef UNORDERED_MAP<uint64, PlayerBotMap*> AcceptedBots;

class PlayerbotMgrOwner
{
    public:
        PlayerbotMgrOwner(Player* const master);
        virtual ~PlayerbotMgrOwner();

        void AddPlayerBot(uint64 guid);
        void LogoutPlayerBot(uint64 guid, bool uninvite = true);
        Player* GetPlayerBot(uint64 guid) const;
        Player* GetMaster() const { return m_master; }

        PlayerBotMap::const_iterator GetPlayerBotsBegin() const { return _playerBotsOwner.begin(); }
        PlayerBotMap::const_iterator GetPlayerBotsEnd()   const { return _playerBotsOwner.end();   }
        PlayerBotMap const& getPlayerbots() const { return _playerBotsOwner; }
        uint8 GetPlayerBotsCount() const { return uint8(_playerBotsOwner.size()); }

        void LogoutAllBots();
        void RemoveAllBotsFromGroup(bool force = false);
        void OnBotLogin(Player* const bot);

        uint16 Rand() const;
        static uint32 GetSpec(Player* player);

        //config variables (needs review)
        float m_confFollowDistance[2];
        //uint32 m_confRestrictBotLevel;
        //uint32 m_confDisableBotsInRealm;
        //uint32 m_confMaxNumBots;
        //bool m_confDisableBots;
        //bool m_confDebugWhisper;
        //uint32 gConfigSellLevelDiff;
        //bool m_confCollectCombat;
        //bool m_confCollectQuest;
        //bool m_confCollectProfession;
        //bool m_confCollectLoot;
        //bool m_confCollectSkin;
        //bool m_confCollectObjects;
        //uint32 m_confCollectDistance;
        //uint32 m_confCollectDistanceMax;

    private:
        Player* const m_master;
        PlayerBotMap _playerBotsOwner;
};

class PlayerbotMgrAcceptor
{
    public:
        PlayerbotMgrAcceptor(Player* const master);
        virtual ~PlayerbotMgrAcceptor();

        void AcceptPlayerBot(Player* bot);
        Player* GetPlayerBot(uint64 guid) const;
        Player* GetAcceptor() const { return _acceptor; }

        AcceptedBots::const_iterator GetAcceptedPlayerBotsBegin() const { return _playerBotsAcceptor.begin(); }
        AcceptedBots::const_iterator GetAcceptedPlayerBotsEnd()   const { return _playerBotsAcceptor.end();   }
        AcceptedBots const& getAcceptedPlayerbots() const { return _playerBotsAcceptor; }
        uint8 GetAcceptedPlayerBotsCount() const { return uint8(_playerBotsAcceptor.size()); }

        void RemoveBotFromMap(uint64 guid);
        void RemoveBotFromGroup(uint64 guid = 0);
        void RemoveAllBotsOnLogout();

    private:
        Player* const _acceptor;
        AcceptedBots _playerBotsAcceptor;
};

#endif
