#ifndef _PLAYERBOTHLP_H
#define _PLAYERBOTHLP_H

#include "Common.h"

class Player;

class PlayerbotHelper
{
    public:
        PlayerbotHelper(Player* const master);
        virtual ~PlayerbotHelper();

        Player* GetOwner() const { return _master; }

        static bool OnGossipHello(Player* player);
        static bool OnGossipSelect(Player* player, uint32 sender, uint32 action);

    private:
        static void SendCreatePlayerBot(Player* player, uint32 action);
        static void SendCreatePlayerBotMenu(Player* player, uint32 /*action*/);
        static void SendRemovePlayerBotAll(Player* player);
        static void SendRemovePlayerBot(Player* player, uint32 action);
        static void SendRemovePlayerBotMenu(Player* player, uint32 /*action*/);

        static void SendRemoveNPCBot(Player* player, uint32 action);
        static void SendRemoveNPCBotMenu(Player* player, uint32 /*action*/);
        static void SendCreateNPCBot(Player* player, uint32 action);
        static void SendCreateNPCBotMenu(Player* player, uint32 /*action*/);


        static void SendManageBotsMenu(Player* player, uint32 action);
        static void SendBotSelectionMenu(Player* player, uint32 senderAction);

        static void SendManageBotsFollowMenu(Player* player, uint32 action);
        static void SendManageBotsFollowSelection(Player* player, uint32 /*action*/);
        static void SendManageBotsFollowName(Player* player, uint32 /*action*/);

        static void SendManageBotsFreeRoamMenu(Player* player, uint32 action);
        static void SendManageBotsFreeRoamSet(Player* player, uint32 action);

        static void SendBotHelpWhisper(Player* player, uint32 /*action*/);
        static std::string GetLocaleStringForTextID(std::string& textValue, uint32 textId, int32 localeIdx = 0);

        static void CreateBotList(Player* player, uint32 action);

        Player* const _master;
};

#endif
