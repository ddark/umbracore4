/*
Playerbot System by Graff (onlysuffering@gmail.com)
Original source: https://github.com/blueboy/portal/commits/new-ai
Type: Rewrite project
Complete: ~10%
TODO:
Everything but this
*/

//#include "Common.h"
#include "ItemPrototype.h"
//#include "World.h"
//#include "SpellMgr.h"
//#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "bp_gridNotfiers.h"
#include "CellImpl.h"
//#include "ProgressBar.h"
#include "Chat.h"
#include "CreatureAIImpl.h"
#include "bp_ai.h"
#include "bp_mgr.h"
#include "bp_dk_ai.h"
#include "bp_dru_ai.h"
#include "bp_hun_ai.h"
#include "bp_mag_ai.h"
#include "bp_pal_ai.h"
#include "bp_pri_ai.h"
#include "bp_rog_ai.h"
#include "bp_sha_ai.h"
#include "bp_warl_ai.h"
#include "bp_warr_ai.h"
#include "InstanceSaveMgr.h"
#include "Player.h"
#include "Group.h"
#include "GroupMgr.h"
#include "Pet.h"
#include "SocialMgr.h"
//#include "ReputationMgr.h"
#include "ObjectMgr.h"
#include "WorldSession.h"
//#include "Spell.h"
//#include "Unit.h"
//#include "SpellAuras.h"
#include "SpellAuraEffects.h"
//#include "SharedDefines.h"
//#include "Log.h"
#include "GossipDef.h"
//#include "MotionMaster.h"
#include "AuctionHouseMgr.h"
//#include "Mail.h"
#include "Guild.h"
//#include "GuildMgr.h"
#include "Language.h"
#include <iomanip>
//#include <iostream>

#define MIN_FOLLOW_DISTANCE         4.f
const uint32 VENDOR_MASK = (UNIT_NPC_FLAG_VENDOR | UNIT_NPC_FLAG_VENDOR_AMMO | UNIT_NPC_FLAG_VENDOR_FOOD | UNIT_NPC_FLAG_VENDOR_POISON | UNIT_NPC_FLAG_VENDOR_REAGENT);
Player::BoundInstancesMap _botBoundInstances[MAX_DIFFICULTY];
Position destPos;

AfterCast::AfterCast()
{
    _afterCastTarget = NULL;
}
inline Unit* AfterCast::GetTarget() const
{
    return _afterCastTarget;
}
inline void AfterCast::SetTarget(Unit* target)
{
    _afterCastTarget = target;
}
inline void AfterCast::SetAfterCastCommand(void (*newAfterCast)(Unit*))
{
    afterCast = newAfterCast;
}
inline void AfterCast::LaunchAfterCastCommand()
{
    ASSERT(_afterCastTarget);

    return (*afterCast)(_afterCastTarget);
}

//Need to remove most of this stuff (or comment for debugging)
class PlayerbotChatHandler : public ChatHandler
{
public:
    explicit PlayerbotChatHandler(WorldSession* masterSession) : ChatHandler(masterSession) {}
    bool revive(Player& botPlayer) { return HandleReviveCommand(this, botPlayer.GetName().c_str()); }
    //bool dropQuest(char const* str) { return HandleQuestRemove(this, str); }
private:
    static bool HandleReviveCommand(ChatHandler* handler, char const* args)
    {
        Player* target;
        uint64 targetGuid;
        std::string targetName;
        if (!handler->extractPlayerTarget((char*)args, &target, &targetGuid, &targetName))
            return false;

        //Player* _player = handler->GetSession()->GetPlayer();
        if (!target || target->isAlive() || target->InArena())
        {
            handler->PSendSysMessage("Resurrection player %s, failed", uint32(GUID_LOPART(targetGuid)));
            handler->SetSentErrorMessage(true);
            return false;
        }

        target->ResurrectPlayer(target->InBattleground() ? 1.0f : 0.5f);
        target->SpawnCorpseBones();

        handler->SetSentErrorMessage(true);
        return true;
    }

    static bool HandleQuestRemove(ChatHandler* handler, const char* args)
    {
        Player* player = handler->getSelectedPlayer();
        if (!player)
        {
            handler->SendSysMessage(LANG_NO_CHAR_SELECTED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // .removequest #entry'
        // number or [name] Shift-click form |color|Hquest:quest_id:quest_level|h[name]|h|r
        char* cId = handler->extractKeyFromLink((char*)args, "Hquest");
        if (!cId)
            return false;

        uint32 entry = atol(cId);

        Quest const* quest = sObjectMgr->GetQuestTemplate(entry);

        if (!quest)
        {
            handler->PSendSysMessage(LANG_COMMAND_QUEST_NOTFOUND, entry);
            handler->SetSentErrorMessage(true);
            return false;
        }

        // remove all quest entries for 'entry' from quest log
        for (uint8 slot = 0; slot < MAX_QUEST_LOG_SIZE; ++slot)
        {
            uint32 logQuest = player->GetQuestSlotQuestId(slot);
            if (logQuest == entry)
            {
                player->SetQuestSlot(slot, 0);

                // we ignore unequippable quest items in this case, its' still be equipped
                player->TakeQuestSourceItem(logQuest, false);
            }
        }

        player->RemoveActiveQuest(entry);
        player->RemoveRewardedQuest(entry);

        handler->SendSysMessage(LANG_COMMAND_QUEST_REMOVED);
        return true;
    }
};

PlayerbotAI::PlayerbotAI(PlayerbotMgrOwner* const mgr, Player* const bot) : _mgr(mgr), me(bot), _classAI(NULL)
//m_CurrentlyCastingSpellId(0), m_CraftSpellId(0), m_spellIdCommand(0), m_targetGuidCommand(0), m_taxiMaster(0)
{
    //m_bDebugCommandChat = _mgr->m_confDebugWhisper;

    //m_targetChanged = false;
    //m_targetType = TARGET_NORMAL;
    //m_targetCombat = 0;
    //m_targetAssist = 0;
    //m_targetProtect = 0;

    // set collection options
    //m_collectionFlags = 0;
    //m_collectDist = _mgr->m_confCollectDistance;
    //if (_mgr->m_confCollectCombat)
    //    SetCollectFlag(COLLECT_FLAG_COMBAT);
    //if (_mgr->m_confCollectQuest)
    //    SetCollectFlag(COLLECT_FLAG_QUEST);
    //if (_mgr->m_confCollectProfession)
    //    SetCollectFlag(COLLECT_FLAG_PROFESSION);
    //if (_mgr->m_confCollectLoot)
    //    SetCollectFlag(COLLECT_FLAG_LOOT);
    //if (_mgr->m_confCollectSkin && m_bot->HasSkill(SKILL_SKINNING))
    //    SetCollectFlag(COLLECT_FLAG_SKIN);
    //if (_mgr->m_confCollectObjects)
    //    SetCollectFlag(COLLECT_FLAG_NEAROBJECT);

    //// set needed item list
    //SetQuestNeedItems();
    //SetQuestNeedCreatures();

    //// start following master (will also teleport bot to master)
    //m_dropWhite = false;
    //m_AutoEquipToggle = false;
    //m_FollowAutoGo = FOLLOWAUTOGO_OFF; //turn on bot auto follow distance can be turned off by player
    //DistOverRide = 0; //set initial adjustable follow settings
    //IsUpOrDown = 0;
    //gTempDist = 0.5f;
    //gTempDist2 = 1.0f;
    //BotDataRestore();
    //ClearActiveTalentSpec();

    _ownerGUID = me->GetSession()->m_master->GetGUID();

    _botStates = 0;
    _combatStates = 0;
    _movementFlags = 0;
    _positioningFlags = 0;

    _followTimer = 0;
    _followTargetGUID = _ownerGUID;
    _attackTargetGUID = 0;

    _prevDest.first = 0.f;
    _prevDest.second = NULL;

    _waitTimer = 0;
    _mountTimer = 0;
    _cureTimer = 0;
    _selfResTimer = 0;
    _sheathTimer = 0;
    _moveTimer = 0;

    _opponent = NULL;
    _afterCast = NULL;

    _canSelfRes = false;

    //get class specific ai
    ReloadClassAI();
    //init motion
    AddBotState(BOTSTATE_FOLLOW);
    //save instance bounds
    for (uint8 i = REGULAR_DIFFICULTY; i != MAX_DIFFICULTY; ++i)
    {
        for (Player::BoundInstancesMap::iterator itr = me->GetBoundInstances(Difficulty(i)).begin(); itr != me->GetBoundInstances(Difficulty(i)).end(); ++itr)
        {
            _botBoundInstances[Difficulty(i)][itr->first].perm = itr->second.perm;
            //reserve current save by creating new (fake) save, any of bot's saves can become invalid in time
            _botBoundInstances[Difficulty(i)][itr->first].save = new InstanceSave(itr->second.save->GetMapId(), itr->second.save->GetInstanceId(), Difficulty(i), itr->second.save->GetResetTime(), itr->second.save->CanReset());
        }
    }

    _UpdateOutrunDistance();
}

PlayerbotAI::~PlayerbotAI()
{
    //restore instance binds (save to db)
    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    PreparedStatement* stmt;
    for (uint8 i = REGULAR_DIFFICULTY; i != MAX_DIFFICULTY; ++i)
    {
        Difficulty diff = Difficulty(i);
        for (Player::BoundInstancesMap::iterator itr = _botBoundInstances[i].begin(); itr != _botBoundInstances[i].end(); ++itr)
        {
            if (me->GetBoundInstance(itr->first, diff))
            {
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_INSTANCE_BY_INSTANCE_GUID);

                stmt->setUInt32(0, me->GetGUIDLow());
                stmt->setUInt32(1, itr->second.save->GetInstanceId());

                trans->Append(stmt);

                //direct unbind is unsafe on lags
                //me->UnbindInstance(itr->first, diff);
            }

            stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_INSTANCE);

            stmt->setUInt32(0, me->GetGUIDLow());
            stmt->setUInt32(1, itr->second.save->GetInstanceId());
            stmt->setBool(2, itr->second.perm);

            trans->Append(stmt);
            //if after unbind old save is not unloaded bind to old save, else create new save and save to instance mgr
            //if (InstanceSave* save = sInstanceSaveMgr->AddInstanceSave(itr->second.save->GetMapId(), itr->second.save->GetInstanceId(), diff, itr->second.save->GetResetTime(), itr->second.save->CanReset(), true))
            //    me->BindToInstance(save, itr->second.perm);

            delete itr->second.save; //delete our old fake save
        }

        _botBoundInstances[diff].clear();
    }

    CharacterDatabase.CommitTransaction(trans);

    if (_classAI) delete _classAI;
    if (_afterCast) delete _afterCast;
}

void PlayerbotAI::ReloadClassAI()
{
    if (_classAI)
        delete _classAI;
    _mySpec = PlayerbotMgrOwner::GetSpec(me);
    switch (me->getClass())
    {
        case CLASS_PRIEST:
            _classAI = (PlayerbotClassAI*) new PlayerbotPriestAI(GetMaster(), me, this);
            break;
        case CLASS_MAGE:
            _classAI = (PlayerbotClassAI*) new PlayerbotMageAI(GetMaster(), me, this);
            break;
        case CLASS_WARLOCK:
            _classAI = (PlayerbotClassAI*) new PlayerbotWarlockAI(GetMaster(), me, this);
            break;
        case CLASS_WARRIOR:
            _classAI = (PlayerbotClassAI*) new PlayerbotWarriorAI(GetMaster(), me, this);
            break;
        case CLASS_SHAMAN:
            _classAI = (PlayerbotClassAI*) new PlayerbotShamanAI(GetMaster(), me, this);
            break;
        case CLASS_PALADIN:
            _classAI = (PlayerbotClassAI*) new PlayerbotPaladinAI(GetMaster(), me, this);
            break;
        case CLASS_ROGUE:
            _classAI = (PlayerbotClassAI*) new PlayerbotRogueAI(GetMaster(), me, this);
            break;
        case CLASS_DRUID:
            _classAI = (PlayerbotClassAI*) new PlayerbotDruidAI(GetMaster(), me, this);
            break;
        case CLASS_HUNTER:
            _classAI = (PlayerbotClassAI*) new PlayerbotHunterAI(GetMaster(), me, this);
            break;
        case CLASS_DEATH_KNIGHT:
            _classAI = (PlayerbotClassAI*) new PlayerbotDeathKnightAI(GetMaster(), me, this);
            break;
    }

    //HERB_GATHERING      = initSpell(HERB_GATHERING_1);
    //MINING              = initSpell(MINING_1);
    //SKINNING            = initSpell(SKINNING_1);
}

void PlayerbotAI::UpdateAI(uint32 diff)
{
    _doTimers(diff);

    //disabled AI check point 1
    if (!_classAI || me->IsBeingTeleported() || !me->IsInWorld() || !me->FindMap())
        return;

    //Global state update
    //TODO
    _UpdatePositioningFlags();

    //debug remove root
    if (!me->HasUnitState(UNIT_STATE_CASTING))
    {
        UnRoot();
        //process aftercast
        //Aftercast should be created in certain cast action
        //TODO: aftercast pack
        //TODO: templated action maybe
        //TODO: move to `OnSpellGo` hook
        if (_afterCast != NULL)
        {
            _afterCast->LaunchAfterCastCommand();
            delete _afterCast;
            _afterCast = NULL;
        }
    }

    if (_waitTimer > diff)
        return;

    _waitTimer = 300 + (_mgr->GetPlayerBotsCount() - 1) * 50;//up to a sec

    //DEATH BEHAVIOUR 100.0%
    if (me->isDead())
    {
        UpdateDeadActions(diff);
        return;
    }

    //disabled AI check point 2
    if (me->isInFlight() || me->isCharmed() || me->GetTrader())
        return;

    //BreakCC(diff);
    if (CCed(me)) return;

    //POTIONS/FLASKS/etc. (opt)

    //CURES / SUPPORT / HEAL | CLASS SPECIFIC
    _classAI->UpdateGroupActions(diff);

    //MISC ACTIONS

    //INCOMBAT UPDATE PAI 80% else is CAI
    if (HasAttackTarget(_classAI->GetDamageSchoolMask()))
    {
        UpdateIncombatActions(diff, _classAI->GetDamageSchoolMask(), _positioningFlags);
        return;
    }
    ClearBotState(BOTSTATE_COMBAT);
    _opponent = NULL;

    //Update loot here
    //if (canUpdateLoot)
    //{
    //    UpdateLootList();
    //    if (!loots.empty())
    //        AddBotState(BOTSTATE_LOOTING);
    //}

    //OUT OF COMBAT BEHAVIOUR 0.1%
    if (HasBotState(BOTSTATE_LOOTING))
    {
        if (_doLoot(diff) != RESULT_DONE)
            return;
        ClearBotState(BOTSTATE_LOOTING);
    }
    else if (!me->isInCombat())
    {
        //TODO: UpdateNonCombatActions(diff);
        //TODO: UpdateRations(diff);

        //TODO: Only if have to follow
        if (_followTargetGUID != me->GetGUID())
            AddBotState(BOTSTATE_FOLLOW);
    }

    if (HasBotState(BOTSTATE_FOLLOW))
        UpdateFollowActions(diff);

    UpdateStandState();

    //DELAYED OPERATIONS

    //TODO: TAME

    //TODO: DIRECT CAST

    //TODO: CRAFTING

    //TODO: ORDERS (opt)
}
//Handles self-resurrection
//Teleports to corpse and then resurrects or uses selfres spells (soulstone)
//Rezzers acception is handled in OnResurrectRequest()
void PlayerbotAI::UpdateDeadActions(uint32 diff)
{
    if (!_canSelfRes)
    {
        _selfResTimer = GetMaster()->isInCombat() ? 2000 : 10000;
        if (Map* map = GetMaster()->FindMap())
            if (map->IsRaidOrHeroicDungeon())
                _selfResTimer /= 2;
        _canSelfRes = true;
        return;
    }

    if (_selfResTimer > diff)
        return;

    if (me->getDeathState() == CORPSE)
    {
        //debug
        //if (me->GetCorpse())
        //{
        //    sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: UpdateDeadActions - %s already has a corpse!", me->GetName().c_str());
        //    //m_bot->setDeathState(DEAD);
        //    return;
        //}
        //clear loot
        //m_lootTargets.clear();
        //m_lootCurrent = 0;

        //has selfRezz
        if (me->GetUInt32Value(PLAYER_SELF_RES_SPELL))
        {
            WorldPacket* packet = new WorldPacket(CMSG_SELF_RES, 8);//not really used
            *packet << me->GetGUID();
            me->GetSession()->QueuePacket(packet);
            //me->GetSession()->HandleSelfResOpcode(p);
            return;
        }

        me->BuildPlayerRepop();
        me->setDeathState(DEAD);
        //me->ResetDeathTimer();//release 
        //me->RepopAtGraveyard();
    }
    //else if (m_bot->getDeathState() == DEAD) //appears to be unused for players
    else if (me->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
    {
        Corpse* corpse = me->GetCorpse();
        //debug
        if (!corpse)
        {
            //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: UpdateAI - %s has no corpse!", me->GetName().c_str());
            //m_bot->setDeathState(CORPSE);
            return;
        }
        //check reclaim distance, teleport if needed
        if (me->GetDistance(corpse) > CORPSE_RECLAIM_RADIUS)
        {
            //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: UpdateAI - Teleport %s to corpse...", me->GetName().c_str());
            me->TeleportTo(*corpse, TELE_TO_GM_MODE);
        }
        //check reclaim delay
        int32 delay = (corpse->GetGhostTime() + me->GetCorpseReclaimDelay(corpse->GetType() == CORPSE_RESURRECTABLE_PVP)) - time(NULL);
        if (delay > 0)
        {
            //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: UpdateAI - %s has to wait for %u seconds to revive...", me->GetName().c_str(), uint32(delay));
            return;
        }
        //revive by master
        //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: UpdateAI - Reviving %s...", me->GetName().c_str());
        if (!PlayerbotChatHandler(GetMaster()->GetSession()).revive(*me))
        {
            //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s could not be revived ...", me->GetName().c_str());
            return;
        }
    }
    _canSelfRes = false;
}

void PlayerbotAI::UpdateIncombatActions(uint32 diff, uint32 dmgmask, uint32 positioningMask)
{
    //Debug
    if (me->IsMounted())
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::UpdateIncombatActions() Error! Bot %s (%u) is mounted! Dismounting...", me->GetName().c_str(), me->GetGUIDLow());
        if (!HasBotState(BOTSTATE_INDEPENDENT))
            UpdateMountedState(0);
        else
            me->RemoveAurasByType(SPELL_AURA_MOUNTED);
    }

    if (!HasBotState(BOTSTATE_COMBAT))
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::UpdateIncombatActions() Error! Bot %s (%u) has no BOTSTATE_COMBAT flag! Adding...", me->GetName().c_str(), me->GetGUIDLow());
        AddBotState(BOTSTATE_COMBAT);
    }

    //SHEATH
    if (!me->HasUnitState(UNIT_STATE_CASTING))
    {
        SheathState sheath = (dmgmask & BOT_DAMAGE_SCHOOL_RANGED) ? SHEATH_STATE_RANGED : SHEATH_STATE_MELEE;
        if (me->GetSheath() != sheath)
        {
            //sheath timer should go at leavecombat
            //_sheathTimer = 1000;
            me->SetSheath(sheath);
        }
    }

    //IMMEDIATE SUPPORT CAI full

    //CC CAI full
    { }

    //handle target
    if (Unit* u = me->getVictim())
    {
        if (!me->HasInArc(M_PI/2, u))
        {
            me->SetOrientation(me->GetAngle(u));
            me->SetFacingTo(me->GetAngle(u));
        }

        SetMovement(diff, u, positioningMask, u->GetCombatReach() * 0.8);

        //ATTACK CAI full
        _classAI->DoCombatActions(diff);

        return;
    }

    //no target
    ClearBotState(BOTSTATE_COMBAT);
}

void PlayerbotAI::UpdateFollowActions(uint32 diff)
{
    if (_followTimer > diff)
        return;

    if (me->HasUnitState(UNIT_STATE_CASTING) || CCed(me, true))
        return;

    if (me->getVictim())
    {
        sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s tried following while still fighting!", me->GetName().c_str());
        return;
    }

    _followTimer = 500 + 100 * _mgr->GetPlayerBotsCount();

    //sheath additive
    if (_sheathTimer <= diff && me->GetSheath() != SHEATH_STATE_UNARMED && _mgr->Rand() < 30)
    {
        _sheathTimer = 1000;
        me->SetSheath(SHEATH_STATE_UNARMED);
    }

    if (_followTargetGUID == 0)
    {
        sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s tried following with no target specified! Setting to master", me->GetName().c_str());
        ClearBotState(BOTSTATE_INDEPENDENT);
        _followTargetGUID = _ownerGUID;
    }

    ASSERT(HasBotState(BOTSTATE_INDEPENDENT) != (_followTargetGUID == _ownerGUID));

    //get follow target:
    //a) follow target set by master
    //b) master
    //if player target is dead use it's corpse as follow target
    WorldObject* fTarget = NULL;
    Player* fPlayer = NULL;
    if (_followTargetGUID == _ownerGUID)
        fPlayer = me->GetSession()->m_master;
    else
    {
        fTarget = ObjectAccessor::FindUnit(_followTargetGUID);
        if (fTarget)
            fPlayer = fTarget->ToPlayer();
        else
        {
            _followTargetGUID = _ownerGUID;
            fPlayer = me->GetSession()->m_master;
        }
    }
    //HERE SHOULD BE "FIND NEW TARGET"
    if (!fPlayer && fTarget && fTarget->ToUnit() && fTarget->ToUnit()->isDead())
    {
        fPlayer = me->GetSession()->m_master;
        fTarget = fPlayer;
    }

    if (fPlayer)
    {
        if (fPlayer->isDead() && fPlayer->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
            fTarget = fPlayer->GetCorpse();
        else
            fTarget = fPlayer;
    }

    //unsafe
    //if (me->GetPhaseMask() != fTarget->GetPhaseMask())
    //    me->SetPhaseMask(fTarget->GetPhaseMask(), true);

    //flight check (all such checks should go here)
    if (!fTarget->IsInWorld() || !fTarget->FindMap() ||
        (fPlayer && (fPlayer->isInFlight() || fPlayer->IsBeingTeleported())))
    {
        _followTimer = 3000;
        return;
    }

    //teleport if too far away
    Map const* const mymap = me->GetMap();
    Map const* const tarmap = fTarget->GetMap();
    if (mymap != tarmap || me->GetDistance2d(fTarget) > _outrunDistance)
    {
        if (fPlayer && tarmap->Instanceable() && mymap != tarmap)
        {
            Difficulty diff = fPlayer->GetDifficulty(tarmap->IsRaid());
            if (InstancePlayerBind* bind = me->GetBoundInstance(tarmap->GetId(), diff))
                _UnbindInstance(tarmap->GetId(), diff);
        }
        me->TeleportTo(*fTarget, TELE_TO_GM_MODE);
        return;
    }

    SetMovement(diff, fTarget, POSITIONING_FOLLOW, MIN_FOLLOW_DISTANCE);

    //only if following master
    if (fPlayer)
        UpdateMountedState(diff);
}

void PlayerbotAI::UpdateStandState()
{
    //only if following master
    if (_followTargetGUID != _ownerGUID)
        return;

    if (GetMaster()->getStandState() == UNIT_STAND_STATE_STAND && me->getStandState() == UNIT_STAND_STATE_SIT &&
        !(me->GetInterruptMask() & AURA_INTERRUPT_FLAG_NOT_SEATED))
        me->SetStandState(UNIT_STAND_STATE_STAND);
    if (GetMaster()->getStandState() == UNIT_STAND_STATE_SIT && me->getStandState() == UNIT_STAND_STATE_STAND &&
        !me->isInCombat() && !me->isMoving() && !me->IsMounted())
        me->SetStandState(UNIT_STAND_STATE_SIT);
}

void PlayerbotAI::UpdateMountedState(uint32 diff)
{
    //HUGE TODO HERE
    //TASK BASED MOVEMENT
    //REQUIRES MOUNTING DECISION

    //only if following master
    if (_followTargetGUID != _ownerGUID)
        return;

    //dismount
    if (!GetMaster()->IsMounted() || ((me->isInCombat() || !me->getAttackers().empty() || _opponent) && HasBotState(BOTSTATE_COMBAT)))
    {
        if (me->HasAuraType(SPELL_AURA_MOUNTED))
            me->RemoveAurasByType(SPELL_AURA_MOUNTED);
        return;
    }

    if (_mountTimer > diff)
        return;

    _mountTimer = 500;

    //cannot mount in water, in combat, if mounted somehow (some encounter)
    if (me->HasUnitMovementFlag(MOVEMENTFLAG_SWIMMING) || me->isInCombat() || me->IsMounted())
        return;

    //TODO: HUGE
    //Player Part
    Unit::AuraEffectList const& auraList = GetMaster()->GetAuraEffectsByType(SPELL_AURA_MOUNTED);
    if (!auraList.empty())
    {
        SpellInfo const* spellInfo = auraList.front()->GetSpellInfo();
        if (!spellInfo)
        {
            sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s tried to mount but master %s is mounted by spell with no spellInfo!", me->GetName().c_str(), GetMaster()->GetName().c_str());
            return;
        }

        //Bot Part
        uint32 spellMount = 0;
        //cheap check if we know this spell
        for (PlayerSpellMap::iterator itr = me->GetSpellMap().begin(); itr != me->GetSpellMap().end(); ++itr)
        {
            //if (itr->second->state == PLAYERSPELL_REMOVED || itr->second->disabled)
            //    continue;

            //cheap check if we just have the same mount
            uint32 spellId = itr->first;
            if (spellInfo->Id == spellId)
            {
                spellMount = spellId;
                break;
            }
        }
        if (!spellMount)
        {
            //analyze and find proper mount spell
            for (PlayerSpellMap::iterator itr = me->GetSpellMap().begin(); itr != me->GetSpellMap().end(); ++itr)
            {
                //if (itr->second->state == PLAYERSPELL_REMOVED || itr->second->disabled)
                //    continue;
                uint32 spellId = itr->first;
                SpellInfo const* bSpellInfo = sSpellMgr->GetSpellInfo(spellId);
                if (!bSpellInfo || bSpellInfo->IsPassive())
                    continue;

                for (uint8 i = 0; i != MAX_SPELL_EFFECTS; ++i)
                {
                    if (bSpellInfo->Effects[i].ApplyAuraName == SPELL_AURA_MOUNTED)
                    {
                        //arrange values
                        int8 j = i-1, k = i+1;
                        if (j < 0)// i == 0
                            j = k+1;//2
                        else if (k >= MAX_SPELL_EFFECTS)// i == 2
                            k = j-1;//0

                        if (bSpellInfo->Effects[j].ApplyAuraName == SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED)
                        {
                            for (uint8 i = 0; i != MAX_SPELL_EFFECTS; ++i)
                            {
                                if (spellInfo->Effects[i].BasePoints == bSpellInfo->Effects[j].BasePoints)
                                {
                                    spellMount = spellId;
                                    break;
                                }
                            }
                            if (spellMount)
                                break;
                        }
                        else if (bSpellInfo->Effects[k].ApplyAuraName == SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED)
                        {
                            for (uint8 i = 0; i != MAX_SPELL_EFFECTS; ++i)
                            {
                                if (spellInfo->Effects[i].BasePoints == bSpellInfo->Effects[k].BasePoints)
                                {
                                    spellMount = spellId;
                                    break;
                                }
                            }
                            if (spellMount)
                                break;
                        }
                        else if (bSpellInfo->Effects[j].ApplyAuraName == SPELL_AURA_MOD_INCREASE_MOUNTED_SPEED)
                        {
                            for (uint8 i = 0; i != MAX_SPELL_EFFECTS; ++i)
                            {
                                if (spellInfo->Effects[i].BasePoints == bSpellInfo->Effects[j].BasePoints)
                                {
                                    spellMount = spellId;
                                    break;
                                }
                            }
                            if (spellMount)
                                break;
                        }
                        else if (bSpellInfo->Effects[k].ApplyAuraName == SPELL_AURA_MOD_INCREASE_MOUNTED_SPEED)
                        {
                            for (uint8 i = 0; i != MAX_SPELL_EFFECTS; ++i)
                            {
                                if (spellInfo->Effects[i].BasePoints == bSpellInfo->Effects[k].BasePoints)
                                {
                                    spellMount = spellId;
                                    break;
                                }
                            }
                            if (spellMount)
                                break;
                        }
                    }
                }
                if (spellMount)
                    break;
            }
        }
        if (spellMount)
        {
            CastSpell(me, spellMount);
        }
        else
        {
            sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s cannot find a proper mount!", me->GetName().c_str());
            //SendWhisper("Cannot find approriate mount!", *GetMaster());
        }
    }
}

bool PlayerbotAI::HasAttackTarget(uint32 dmgmask)
{
    //choose proper targeting mode
    //step 1: create damageschoolmask DEPRECTATED
    //step 2: check conditions

    //TODO: wand (casters) / melee (hunter) and add to mask
    //switch (myclass)
    //{
    ////wand users
    //case CLASS_WARLOCK:
    //case CLASS_PRIEST:
    //case CLASS_MAGE:
    //{
    //    if (GetManaPCT(me) < 5)
    //    {
    //    }
    //}

    //}

    bool reset = false;
    _opponent = GetAttackTarget(dmgmask, reset);

    if (!_opponent)
    {
        me->AttackStop();
        return false;
    }

    AddBotState(BOTSTATE_COMBAT);
    if (_opponent->GetTypeId() == TYPEID_PLAYER)
        AddCombatState(COMBAT_STATE_PVP);
    ClearBotState(BOTSTATE_FOLLOW);
    if (reset)
        AddCombatState(COMBAT_STATE_RESET);

    if (_opponent != me->getVictim())
        me->Attack(_opponent, true); //add melee attack state anyways

    return true;
}

Unit* PlayerbotAI::GetAttackTarget(uint32 dmgmask, bool &reset) const
{
    //TODO: this crap needs to be reforged to separate free roam and depended modes
    //Duel check, should come after common target
    //here we don't check anything (yet)
    if (me->duel && HasCombatState(COMBAT_STATE_DUEL))
        return me->duel->opponent;


    //Check if need to assist master
    Unit* u = GetMaster()->getVictim();
    Unit* mytar = me->getVictim();

    //1) heck for common target
    //this also include independent mode (freeroam) cuz master == me
    //if whithin visible range stay on this target, else find new
    if (u && u == mytar && me->GetDistance2d(u) < MAX_AGGRO_RADIUS + 15.f)
    {
        //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s continues attack common target %s", me->GetName().c_str(), u->GetName().c_str());
        return u;
    }

    //Follow if...
    //only when master != me
    if (!HasBotState(BOTSTATE_INDEPENDENT))
    {
        float followdist = MAX_AGGRO_RADIUS + 15.f;//60 yrds
        //if master is dead (or not ressed yet) chase target to the last
        if (!u && mytar && GetMaster()->isAlive() && (me->GetDistance(mytar) > followdist))
        {
            sLog->outError(LOG_FILTER_PLAYER, "bot %s cannot attack target %s, too far away", me->GetName().c_str(), mytar ? mytar->GetName().c_str() : "none");
            return NULL;
        }
    }

    //2) accuring master's target (new target, if have no target or attacking something else)
    //only when master != me
    if (u && (GetMaster()->isInCombat() || u->isInCombat()) && !IsUnitInDuel(u, GetMaster()) && !IsUnitInPlayersParty(u, GetMaster()))
    {
        //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s starts attack master's target %s", me->GetName().c_str(), u->GetName().c_str());
        return u;
    }
    //3) master's casting or idle - so check if can still attack cur target
    if (CanPlayerbotAttack(me, mytar, dmgmask)/* && !IsUnitInDuel(mytar, GetMaster())*/)
    {
        //if (me->GetDistance(mytar) > (ranged ? 20.f : 5.f) && m_botCommandState != COMMAND_STAY && m_botCommandState != COMMAND_FOLLOW)
        reset = (me->GetDistance(mytar) > GetMinAttackRange(mytar));
        //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s continues attack its target %s (reset = %u)", me->GetName().c_str(), mytar->GetName().c_str(), uint8(reset));
        return mytar;
    }

    //if (followdist == 0 && master->isAlive())
    //    return NULL; //do not bother

    if (!HasBotState(BOTSTATE_INDEPENDENT))
    {
        //Check group
        //1) create playerlist
        std::set<Player*> playerSet;
        playerSet.insert(GetMaster());
        Group const* gr = GetMaster()->GetGroup();
        if (!gr)
        {
            for (PlayerBotMap::const_iterator itr = _mgr->GetPlayerBotsBegin(); itr != _mgr->GetPlayerBotsEnd(); ++itr)
            {
                Player* pl = itr->second;
                if (!pl || !pl->IsInWorld() || pl->IsBeingTeleported() || me->GetMap() != pl->FindMap() || !pl->InSamePhase(me))
                    continue;
                playerSet.insert(itr->second);
            }
        }
        else
        {
            for (GroupReference const* ref = gr->GetFirstMember(); ref != NULL; ref = ref->next())
            {
                Player* pl = ref->getSource();
                if (!pl || !pl->IsInWorld() || pl->IsBeingTeleported() || me->GetMap() != pl->FindMap() || !pl->InSamePhase(me))
                    continue;
                if (pl != GetMaster())
                    playerSet.insert(pl);
                for (PlayerBotMap::const_iterator itr = _mgr->GetPlayerBotsBegin(); itr != _mgr->GetPlayerBotsEnd(); ++itr)
                {
                    pl = itr->second;
                    if (!pl || !pl->IsInWorld() || pl->IsBeingTeleported() || me->GetMap() != pl->FindMap() || !pl->InSamePhase(me))
                        continue;
                    playerSet.insert(itr->second);
                }
            }
        }
        //2) cycle through to find proper target; check players and their npcbots (and their bot pets)
        for (std::set<Player*>::const_iterator itr = playerSet.begin(); itr != playerSet.end(); ++itr)
        {
            Player* pl = (*itr);
            u = pl->getVictim();
            if (u && pl != GetMaster() && CanPlayerbotAttack(me, u, dmgmask) && (pl->isInCombat() || u->isInCombat()))
            {
                //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s hooked %s's victim %s", me->GetName().c_str(), pl->GetName().c_str(), u->GetName().c_str());
                return u;
            }
            if (!pl->HaveBot()) continue;
            for (uint8 i = 0; i != pl->GetMaxNpcBots(); ++i)
            {
                Creature* bot = pl->GetBotMap(i)->_Cre();
                if (!bot || !bot->IsInWorld() || me->GetMap() != bot->FindMap() || !bot->InSamePhase(me))
                    continue;
                u = bot->getVictim();
                if (u && CanPlayerbotAttack(me, u, dmgmask) && (bot->isInCombat() || u->isInCombat()))
                {
                    //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s hooked %s's victim %s", me->GetName().c_str(), bot->GetName().c_str(), u->GetName().c_str());
                    return u;
                }
                Creature* pet = bot->GetIAmABot() ? bot->GetBotsPet() : NULL;
                if (!pet || !pet->InSamePhase(me)) continue;
                u = pet->getVictim();
                if (u && CanPlayerbotAttack(me, u, dmgmask) && (pet->isInCombat() || u->isInCombat()))
                {
                    //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s hooked %s's victim %s", me->GetName().c_str(), pet->GetName().c_str(), u->GetName().c_str());
                    return u;
                }
            }
        }
    }

    //Check targets around
    Unit* t = NULL;
    float maxdist = HasBotState(BOTSTATE_INDEPENDENT) ? MAX_AGGRO_RADIUS + 15.f : MAX_AGGRO_RADIUS;
    //first cycle we search non-cced target, then, if not found, check all
    for (uint8 i = 0; i != 2 && !t; ++i)
    {
        bool attackCC = bool(i);
        PBNearestHostileUnitCheck check(me, maxdist, dmgmask, attackCC);
        Trinity::UnitLastSearcher <PBNearestHostileUnitCheck> searcher(me, t, check);
        me->VisitNearbyObject(maxdist, searcher);
    }

    if (t && _opponent && t != _opponent)
    {
        //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::GetAttackTarget(): bot %s has Found a new target %s", me->GetName().c_str(), t->GetName().c_str());
        reset = true;
    }

    return t;
}

void PlayerbotAI::SetMovement(uint32 diff, WorldObject* followTarget, uint32 positioningMask, float mindistance, float needarc)
{
    if (_moveTimer > diff)
        return;

    if (!followTarget || !followTarget->IsInWorld() || me->GetMap() != followTarget->FindMap())
        return;

    if (me->HasUnitMovementFlag(MOVEMENTFLAG_FALLING))
        return;

    //outrun case should be handled outside, with teleport
    if (me->GetDistance(followTarget) > _outrunDistance)
        return;

    if (CCed(me, true))
        return;

    if ((positioningMask & POSITIONING_ATTACK) && !followTarget->ToUnit())
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::SetMovement() Error! Trying to attack a non-unit object %u (low: %u). Removing POSITIONING_ATTACK flag", followTarget->GetGUID(), followTarget->GetGUIDLow());
        positioningMask &= ~POSITIONING_ATTACK;
    }

    //can we actually move?
    bool canMove = (positioningMask & POSITIONING_ATTACK) ? !HasCombatState(COMBAT_STATE_STAY) : !HasMovementFlag(MOVEMENT_FLAG_HOLD_GROUND);
    float dist = me->GetDistance2d(followTarget);
    if (canMove == false || dist <= mindistance)
    {
        if (canMove && (positioningMask & POSITIONING_ATTACK) && !me->HasUnitState(UNIT_STATE_CASTING))
        {
            if (_MoveBehind(followTarget->ToUnit()))
            {
                _attackTargetGUID = followTarget->ToUnit()->GetGUID();
                return;
            }
        }
        if (needarc != 0.f)
        {
            if (!me->HasInArc(needarc, followTarget))
            {
                me->SetOrientation(me->GetAngle(followTarget));
                me->SetFacingTo(me->GetAngle(followTarget));
                return;
            }
        }
        //return;
    }

    if (me->HasUnitState(UNIT_STATE_CASTING))
        return;

    bool meleeattack = ((positioningMask & POSITIONING_ATTACK) && (positioningMask & POSITIONING_MELEE));
    //pointed movement is of no use in close combat so let melees chase target
    if (meleeattack && !me->HasUnitState(UNIT_STATE_CHASE) && followTarget->ToUnit()->isMoving() && me->IsWithinMeleeRange(followTarget->ToUnit(), ATTACK_DISTANCE - (4.f + 0.35f * (me->GetMeleeReach() + followTarget->ToUnit()->GetMeleeReach())))
        && followTarget->IsWithinLOSInMap(me))
    {
        me->GetMotionMaster()->MoveChase(followTarget->ToUnit());
        return;
    }

    //prevent movement calls spam 
    //ex. 30 yrds timer = 30/2*100 = 15*100=1500 = 1.5sec
    //for 10 yrds timer = 0.5 sec
    //60 yerds - 3 sec
    //maybe this amount should be increased... ?
    //melee attacking is exception if target is in attack range or somewhere close
    if (!(positioningMask & (POSITIONING_ATTACK | POSITIONING_MELEE)) || dist > 3.5f * followTarget->ToUnit()->GetCombatReach())
        _moveTimer = std::max<uint32>(uint32(dist * 0.5f * 100.f), uint32(500));

    _CalculatePos(me, followTarget, positioningMask);

    if (me->GetDistance(destPos) > MIN_FOLLOW_DISTANCE ||
        ((positioningMask & POSITIONING_ATTACK) && !me->IsWithinMeleeRange(followTarget->ToUnit())))
    {
        //do not generate path for melee attackers at close point, this is useless
        me->GetMotionMaster()->MovePoint(me->GetMapId(), destPos.m_positionX, destPos.m_positionY, destPos.m_positionZ, !meleeattack);

        //save pointer for rotation. See OnStopMoving()
        if (positioningMask & POSITIONING_ATTACK)
            _attackTargetGUID = followTarget->ToUnit()->GetGUID();
    }
    else if ((positioningMask & POSITIONING_ATTACK) && !me->HasInArc(2*M_PI/3, followTarget))
    {
        me->SetOrientation(me->GetAngle(followTarget));
        me->SetFacingTo(me->GetAngle(followTarget));
    }
}
//Calculates near position of given target
//Angle reaches 0 at character's facing direction and PI behind
//TODO: proper follow distance calculation
void PlayerbotAI::_CalculatePos(Player const* bot, WorldObject* target, uint32 positioningMask)
{
    //ensure that no mistakes are made
    //we do not check ranged tanking because it is possible (BC Kael'thas Sunstrider)
    ASSERT(bot && target && bot->IsPlayerBot());
    if (positioningMask & POSITIONING_ATTACK)
        ASSERT(target->ToUnit());

    //sLog->outError(LOG_FILTER_PLAYER, "PAI::_CalculatePos() for bot %s...", bot->GetName().c_str());

    bool free = bot->GetPlayerbotAI()->HasBotState(BOTSTATE_INDEPENDENT);
    //sLog->outError(LOG_FILTER_PLAYER, "freemode = %u", uint32(free));
    LastDestination& dest = bot->GetPlayerbotAI()->GetLastDestination();
    //sLog->outError(LOG_FILTER_PLAYER, "prevDestination: %f, %s", dest.first, dest.second ? dest.second->GetName() : "");
    float basedist = bot->GetSession()->m_master->GetPlayerbotMgrOwner()->m_confFollowDistance[0];
    //sLog->outError(LOG_FILTER_PLAYER, "basedist = %f", basedist);
    basedist = std::max<float>(0.f, basedist);
    float dist = basedist;
    float angle = M_PI/2;
    if (positioningMask & POSITIONING_TANKING)
    {
        if (positioningMask & POSITIONING_ATTACK) //attacking our tanking target
        {
            if (positioningMask & POSITIONING_RANGED) //woah ranged tanking
            {
                //any angle is acceptable (for further LoS checks)
                angle = target->GetAngle(bot);
                //prevent outrun if tanking (we are not kiting here)
                dist = std::min<float>(bot->GetDistance2d(target), PlayerbotAI::GetMinAttackRange(target->ToUnit(), true));
            }
            else if (positioningMask & POSITIONING_MELEE) //normal tank'n'spank
            {
                //melee attack
                angle = target->GetAngle(bot);
                dist = 0.f;
            }
            else //unknown tanking attack mode
            {
                sLog->outError(LOG_FILTER_PLAYER, "PAI::_CalculatePos() Error! Wrong positioning mask for tanking attack is given to bot %s (%u)", bot->GetName().c_str(), bot->GetGUIDLow());
            }
        }
        else if (positioningMask & POSITIONING_FOLLOW)//tank following mode
        {
            angle = M_PI / frand(-6.f, 6.f); //step forward 66`
            dist = basedist + basedist * 0.4f + 2.f;
        }
        else //unknown tanking mode
        {
            sLog->outError(LOG_FILTER_PLAYER, "PAI::_CalculatePos() Error! Wrong positioning mask for tanking is given to bot %s (%u)", bot->GetName().c_str(), bot->GetGUIDLow());
        }
    }
    else if (positioningMask & POSITIONING_ATTACK) //attacking as damage dealer
    {
        if (positioningMask & POSITIONING_RANGED)
        {
            //any angle is acceptable (for further LoS checks)
            angle = target->GetAngle(bot);
            //prevent outrun if tanking (we are not kiting here)
            dist = std::min<float>(bot->GetDistance2d(target), PlayerbotAI::GetMinAttackRange(target->ToUnit(), true));

        }
        else if (positioningMask & POSITIONING_MELEE)
        {
            angle = target->GetAngle(bot);
            dist = 0.f;
        }
        else //unknown damagedeal attack positioning
        {
            sLog->outError(LOG_FILTER_PLAYER, "PAI::_CalculatePos() Error! Wrong positioning mask for attack is given to bot %s (%u)", bot->GetName().c_str(), bot->GetGUIDLow());
        }
    }
    else if (positioningMask & POSITIONING_FOLLOW) //normal following
    {
        if (positioningMask & POSITIONING_RANGED) //ranged classes
        {
            angle = (M_PI/4.f) * frand(3.f, 5.f); //stay behind 135`-225`
            dist = frand(0.f, basedist + 3.f);
        }
        else
        {
            //stand to left of right side
            //+-(90`+-27.5`)
            angle = (M_PI/2.f) * RAND(-1.f,1.f) + M_PI * frand(-0.143f, 0.143f);//M_PI/7
            dist = basedist;
            //sLog->outError(LOG_FILTER_PLAYER, "flags = POSITIONING_FOLLOW | POSITIONING_MELEE");
            //sLog->outError(LOG_FILTER_PLAYER, "angle = %f, dist = %f", angle, dist);
        }
    }
    else if (positioningMask & POSITIONING_NONE)
    {
    }
    else if (!positioningMask)
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::_CalculatePos() Error! No positioning mask is given to bot %s (%u)", bot->GetName().c_str(), bot->GetGUIDLow());
        _CalculatePos(bot, target, POSITIONING_FOLLOW);
        return;
    }
    else
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::_CalculatePos() Error! Wrong positioning mask is given to bot %s (%u). Mask = %u", bot->GetName().c_str(), bot->GetGUIDLow(), positioningMask);
        _CalculatePos(bot, target, POSITIONING_FOLLOW);
        return;
    }

    //target changed or angle changed too much
    if (dest.second != target || (abs(abs(dest.first) - abs(angle)) > M_PI/3.f))
    {
        dest.first = angle;
        //if (abs(abs(dest.first) - abs(angle)) > M_PI/3.f)
        //    sLog->outError(LOG_FILTER_PLAYER, "new angle differs too much: old = %f, new = %f, oldangle = newangle", dest.first, angle);
        //else
        //    sLog->outError(LOG_FILTER_PLAYER, "new target != old target, oldangle = newangle");
    }
    else
    {
        angle = dest.first;
        //sLog->outError(LOG_FILTER_PLAYER, "retained old angle");
    }

    dest.second = target;

    //while following we should get in position regarding to mover
    if (!(positioningMask & POSITIONING_ATTACK))
    {
        angle += target->GetOrientation();
        //sLog->outError(LOG_FILTER_PLAYER, "no attack flag, adding orientation %f, angle = %f", target->GetOrientation(), angle);
    }

    float x(0),y(0),z(0);
    float size = bot->GetObjectSize()/3.f;
    bool over = false;

    //from npcbotAI
    if (positioningMask & POSITIONING_ATTACK) //attack position
    {
        float clockwise = RAND(1.f,-1.f);
        for (uint8 i = 0; i != 5; ++i)
        {
            target->GetNearPoint(bot, x, y, z, size, dist, angle);
            bool toofaraway = !free ? bot->GetPlayerbotAI()->GetMaster()->GetDistance(x,y,z) > MAX_AGGRO_RADIUS + 15.f : false;
            bool outoflos = !target->IsWithinLOS(x,y,z);
            if (toofaraway || outoflos)
            {
                if (toofaraway)
                    angle = target->GetAngle(bot->GetPlayerbotAI()->GetMaster()) + frand(0.f, M_PI/2.f) * clockwise;
                if (outoflos)
                    dist *= 0.5f;
            }
            else
            {
                //dist *= 0.75f;
                break;
            }
        }
    }
    else //follow position
    {
        for (uint8 i = 0; i != 5 + over; ++i)
        {
            if (over)
            {
                dist *= 0.2f;
                break;
            }
            target->GetNearPoint(bot, x, y, z, size, dist, angle);
            if (!(positioningMask & POSITIONING_NONE) && !target->IsWithinLOS(x,y,z)) //try to get much closer to target
            {
                if (dist < 0.1f)
                    dist = 0.f;
                else
                    dist *= 0.4f - float(i*0.078f); // if i == 5 mydist*=0.01
                size *= 0.1f;
                if (size < 0.1)
                    size = 0.f;
                //we are at much lower point, raise it up
                if (size == 0.f)
                {
                    if (bot->GetPositionZ() - target->GetPositionZ() < -1.5f)
                        z += 1.5f; //prevent going underground for good
                    else if (bot->GetPositionZ() - target->GetPositionZ() < 0.5f)
                        z += 0.5f; //prevent going underground
                    else if (bot->GetPositionZ() - target->GetPositionZ() < 0.25f)
                        z += 0.25f; //prevent going underground
                }
            }
            else
                over = true;
        }
    }

    destPos.m_positionX = x;
    destPos.m_positionY = y;
    destPos.m_positionZ = z;
    destPos.m_orientation = angle;
}

Player const* PlayerbotAI::_FindMainTank(Group const* group)
{
    ASSERT(group);
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MAINTANK);
    stmt->setUInt32(0, group->GetGUID());
    if (PreparedQueryResult result = CharacterDatabase.Query(stmt))
    {
        do
        {
            Field* field = result->Fetch();
            uint32 lowGuid = field[0].GetInt32();
            uint8 flags = field[1].GetInt8();
            if (flags & MEMBER_FLAG_MAINTANK)
            {
                Group::MemberSlotList const &members = group->GetMemberSlots();
                for (Group::MemberSlotList::const_iterator itr = members.begin(); itr != members.end(); ++itr)
                    if (GUID_LOPART(itr->guid) == lowGuid)
                        return ObjectAccessor::FindPlayer(itr->guid);
            }
        } while (result->NextRow());
    }

    return NULL;
}

uint32 PlayerbotAI::InitSpell(Player* caster, uint32 baseSpell)
{
    if (baseSpell == 0 || !caster || !caster->HasSpell(baseSpell))
        return 0;

    uint32 next = 0;
    SpellChainNode const* Node = sSpellMgr->GetSpellChainNode(baseSpell);
    next = Node && Node->next ? Node->next->Id : 0;

    if (next == 0 || !caster->HasSpell(next)) //next spell in chain doesn't exist or unavailable
        return baseSpell;

    //SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(baseSpell);
    //sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI::InitSpell() proceed spell %u (%s): forwarding to %u (%s)", baseSpell, spellInfo->SpellName[0], next, sSpellMgr->GetSpellInfo(next)->SpellName[0]);
    return InitSpell(caster, next);
}

bool PlayerbotAI::CastSpell(Unit* victim, uint32 spellId) const
{
    return CastSpell(victim, sSpellMgr->GetSpellInfo(spellId));
}

bool PlayerbotAI::CastSpell(Unit* victim, SpellInfo const* spellInfo) const
{
    ASSERT(victim);

    if (!spellInfo)
        return false;
    if (!victim->IsInWorld() || me->GetPhaseMask() != victim->GetPhaseMask() || me->GetMap() != victim->FindMap())
        return false;
    //cooldown
    if (me->HasSpellCooldown(spellInfo->Id))
        return false;
    //GCD
    if (me->GetGlobalCooldownMgr().HasGlobalCooldown(spellInfo))
        return false;
    //power cost
    if (int32(me->GetPower(Powers(spellInfo->PowerType))) < spellInfo->CalcPowerCost(me, spellInfo->GetSchoolMask()))
        return false;
    //range
    bool friendly = me->IsFriendlyTo(victim);
    if (me->GetDistance(victim) > spellInfo->GetMaxRange(friendly, me))
        return false;
    //debug
    if (spellInfo->HasEffect(SPELL_EFFECT_OPEN_LOCK) ||
        spellInfo->HasEffect(SPELL_EFFECT_SKINNING) ||
        spellInfo->HasEffect(SPELL_EFFECT_ENCHANT_ITEM) ||
        spellInfo->HasEffect(SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY))
    {
        sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s trying to cast wrong spell on unit! %s (%u) on %s", me->GetName().c_str(), spellInfo->SpellName[uint8(GetMaster()->GetSession()->GetSessionDbcLocale())], spellInfo->Id, victim->GetName().c_str());
        //return false;
    }

    sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s casts: %s (%u) on %s", me->GetName().c_str(), spellInfo->SpellName[uint8(GetMaster()->GetSession()->GetSessionDbcLocale())], spellInfo->Id, victim->GetName().c_str());

    //everything checked, prepare character for cast
    //LoS - cheaty ;)
    if (friendly && me->IsWithinLOSInMap(victim))
        me->Relocate(victim);
    //face target - just in case
    if (victim != me/* && !me->HasInArc(M_PI * 0.25f, victim)*/)
    {
        me->SetFacingToObject(victim);
        me->SendUpdateToPlayer(GetMaster());
    }

    SpellCastTargets targets;
    targets.SetUnitTarget(victim);
    Spell* spell = new Spell(me, spellInfo, TRIGGERED_NONE);

    //stop moving
    if (spellInfo->CalcCastTime(me, spell) != 0)
        Root();

    spell->prepare(&targets);
    //delete spell;

    //me->CastSpell(victim, spellInfo);
    //if (spell->CheckCast(true) != SPELL_CAST_OK)
    //{
    //    spell->finish(false);
    //    delete spell;
    //    return false;
    //}
    //SpellCastTargets targets;
    //targets.SetUnitTarget(victim);
    //spell->prepare(&targets);

    //check cast success
    if (GetCurrentSpell() == spell)
    {
        //after cast action setup
        //SHOULD BE MOVED TO CERTAIN AI
        //if (afterCast && _afterCast == NULL)
        //{
        //    _afterCast = new AfterCast();
        //    _afterCast->SetTarget(afterTarget);
        //    _afterCast->SetAfterCastCommand(afterCast);
        //}
        return true;
    }

    return false;
}

bool PlayerbotAI::CastSpell(Item* item, uint32 spellId) const
{
    return CastSpell(item, sSpellMgr->GetSpellInfo(spellId));
}

bool PlayerbotAI::CastSpell(Item* item, SpellInfo const* spellInfo) const
{
    ASSERT(item);

    if (!spellInfo || me->HasSpellCooldown(spellInfo->Id) ||
        me->GetGlobalCooldownMgr().HasGlobalCooldown(spellInfo) ||
        int32(me->GetPower(Powers(spellInfo->PowerType))) < spellInfo->CalcPowerCost(me, spellInfo->GetSchoolMask()))
        return false;
    //debug
    if (!(spellInfo->HasEffect(SPELL_EFFECT_ENCHANT_ITEM) ||
        spellInfo->HasEffect(SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY) ||
        spellInfo->HasEffect(SPELL_EFFECT_DISENCHANT)))
    {
        sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s trying to cast wrong spell on item! %s (%u) on %s", me->GetName().c_str(), spellInfo->SpellName[uint8(GetMaster()->GetSession()->GetSessionDbcLocale())], spellInfo->Id, item->GetTemplate()->Name1.c_str());
        return false;
    }

    sLog->outError(LOG_FILTER_PLAYER, "PlayerbotAI: %s casts: %s (%u) on %s (item)", me->GetName().c_str(), spellInfo->SpellName[uint8(GetMaster()->GetSession()->GetSessionDbcLocale())], spellInfo->Id, item->GetTemplate()->Name1.c_str());

    Spell* spell = new Spell(me, spellInfo, TRIGGERED_NONE);

    if (spellInfo->CalcCastTime(me, spell) != 0)
        Root();

    delete spell;

    SpellCastTargets targets;
    targets.SetItemTarget(item);
    targets.SetUnitTarget(item->GetOwner());
    me->CastSpell(targets, spellInfo, NULL);

    return (GetCurrentSpell() == spell);
}

void PlayerbotAI::CureGroup(Player* player, uint32 cureSpell, const uint32 diff)
{
    if (!cureSpell || !player)
        return;

    if (_cureTimer > diff)
        return;

    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(cureSpell);
    if (!spellInfo)
        return;
    if (me->HasSpellCooldown(spellInfo->Id))
        return;
    if (me->GetGlobalCooldownMgr().HasGlobalCooldown(spellInfo))
        return;
    if (int32(me->GetPower(Powers(spellInfo->PowerType))) < spellInfo->CalcPowerCost(me, spellInfo->GetSchoolMask()))
        return;
    if (!IsUnitInPlayersParty(player, GetMaster()))
        return;

    Group const* gr = player->GetGroup();
    if (!gr)
    {
        if (_CureTarget(player, spellInfo))
            return;
        if (Pet* pet = player->GetPet())
            if (_CureTarget(pet, spellInfo))
                return;
        if (!player->HaveBot())
            return;

        for (uint8 i = 0; i != player->GetMaxNpcBots(); ++i)
            if (_CureTarget(player->GetBotMap(i)->_Cre(), spellInfo))
                return;
    }
    else
    {
        bool bots = false;
        Player* tPlayer = NULL;
        for (GroupReference const* itr = gr->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            tPlayer = itr->getSource();
            if (!tPlayer || (!tPlayer->isAlive() && !tPlayer->HaveBot()))
                continue;
            if (!bots && tPlayer->HaveBot())
                bots = true;
            if (_CureTarget(tPlayer, spellInfo))
                return;
            if (Pet* pet = tPlayer->GetPet())
                if (_CureTarget(pet, spellInfo))
                    return;
        }
        if (bots)
        {
            for (GroupReference const* itr = gr->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                tPlayer = itr->getSource();
                if (!tPlayer || !tPlayer->HaveBot())
                    continue;
                for (uint8 i = 0; i != tPlayer->GetMaxNpcBots(); ++i)
                    if (_CureTarget(tPlayer->GetBotMap(i)->_Cre(), spellInfo))
                        return;
            }
        }
    }

    //if no target found (basically) set check delay to prevent useless cycle
    _cureTimer = 2000;
}

inline bool PlayerbotAI::_CureTarget(Unit* target, SpellInfo const* curespellInfo) const
{
    return _CanCureTarget(target, curespellInfo) ? CastSpell(target, curespellInfo) : false;
}

bool PlayerbotAI::_CanCureTarget(Unit* target, SpellInfo const* curespellInfo) const
{
    if (!target || !curespellInfo)
        return false;

    if (!target->isAlive())
        return false;

    if (!target->IsInWorld() ||
        me->GetMap() != target->FindMap() ||
        me->GetPhaseMask() != target->GetPhaseMask())
         return false;

    if (Player* player = target->ToPlayer())
        if (player->IsBeingTeleported())
            return false;

    if (me->GetDistance(target) > curespellInfo->RangeEntry->maxRangeFriend)
        return false;

    if (!IsUnitInPlayersParty(target, GetMaster()))
        return false;

    uint32 dispelMask = 0;
    for (uint8 i = 0; i != MAX_SPELL_EFFECTS; ++i)
        if (curespellInfo->Effects[i].Effect == SPELL_EFFECT_DISPEL)
            dispelMask |= SpellInfo::GetDispelMask(DispelType(curespellInfo->Effects[i].MiscValue));

    if (dispelMask == 0)
        return false;

    DispelChargesList dispel_list;
    target->GetDispellableAuraList(me, dispelMask, dispel_list);

    return (!dispel_list.empty());
}

void PlayerbotAI::HandleTeleportAck()
{
    //me->GetMotionMaster()->Clear(true);
    if (me->IsBeingTeleportedNear())
    {
        WorldPacket p = WorldPacket(MSG_MOVE_TELEPORT_ACK, 8 + 4 + 4);
        p.appendPackGUID(me->GetGUID());
        p << uint32(0); // cast flags
        p << uint32(time(NULL)); // time - not used
        me->GetSession()->HandleMoveTeleportAck(p);
    }
    else if (me->IsBeingTeleportedFar())
        me->GetSession()->HandleMoveWorldportAckOpcode();
}

bool PlayerbotAI::CanPlayerbotAttack(Player const* bot, Unit* target, uint32 mask)
{
    if (!target || !bot || !bot->IsPlayerBot()) return false;
    Player* master = bot->GetPlayerbotAI()->GetMaster();

    if (!target->IsVisible() || !target->isTargetableForAttack() || IsUnitInPlayersParty(target, master))
        return false;

    if (!mask)
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::CanPlayerbotAttack: no damage mask is provided for player %s (%u)!", bot->GetName().c_str(), bot->GetGUIDLow());
        return false;
    }

    //remove ranged damage mask, it cannot be used here (no override)
    if (mask & BOT_DAMAGE_SCHOOL_MASK_RANGED)
        mask &= ~BOT_DAMAGE_SCHOOL_MASK_RANGED;

    uint32 immunity = 0;
    //from Unit.cpp modified IsImmunedToDamage(SpellSchoolMask)
    SpellImmuneList const& schoolList = target->m_spellImmune[IMMUNITY_SCHOOL];
    for (SpellImmuneList::const_iterator itr = schoolList.begin(); itr != schoolList.end(); ++itr)
        immunity |= itr->type;

    SpellImmuneList const& damageList = target->m_spellImmune[IMMUNITY_DAMAGE];
    for (SpellImmuneList::const_iterator itr = damageList.begin(); itr != damageList.end(); ++itr)
        if (!(immunity & itr->type)) //not included
            immunity |= itr->type;

    if (!(mask & ~immunity)) //target is not vulnerable to any provided damage type
        return false;

    if (master != bot)
        return (target->IsHostileTo(master) || target->IsHostileTo(bot) || target->GetReactionTo(master) < REP_FRIENDLY);
    else
        return (target->IsHostileTo(master) || target->GetReactionTo(master) < REP_FRIENDLY);
}
//Checks if 'target' can be attacked (searching target in crowd)
//Used to prevent targeting in duel if they have a tie to bot's party
bool PlayerbotAI::IsUnitInDuel(Unit* target, Player* master)
{
    ASSERT(target && master);
    Player* player = target->GetTypeId() == TYPEID_PLAYER ? target->ToPlayer() : NULL;
    if (!player)
    {
        if (!target->IsControlledByPlayer())
            return false;
        player = target->GetCharmerOrOwnerPlayerOrPlayerItself();
    }

    return (player && player->duel && (IsUnitInPlayersParty(player, master) || IsUnitInPlayersParty(player->duel->opponent, master)));
}

bool PlayerbotAI::IsUnitInPlayersParty(Unit* unit, Player* master/*checker*/)
{
    ASSERT(unit && master);
    if (unit == master)
        return true;
    if (unit->GetReactionTo(master) < REP_FRIENDLY)
        return false;

    Group const* gr = master->GetGroup();

    //free roam
    if (master->IsPlayerBot() && master->GetPlayerbotAI()->HasBotState(BOTSTATE_INDEPENDENT))
    {
        //follow target is automatically threated as protect target
        if (uint64 followTargetGUID = master->GetPlayerbotAI()->GetFollowTargetGuid())
            if (followTargetGUID == unit->GetOwnerGUID() || followTargetGUID == unit->GetGUID())
                return true;
        //bot's minion
        if (!gr)
            return unit->GetOwnerGUID() == master->GetGUID();
        //creature belongs to party member
        if (Creature* cre = unit->ToCreature())
            return gr && gr->IsMember(unit->GetOwnerGUID());
        //party members
        return gr->IsMember(unit->GetGUID());
    }
    else if (master->IsPlayerBot())
    {
        sLog->outError(LOG_FILTER_PLAYER, "Error! PAI::IsUnitInPlayersParty call for bot %s (%u) whithout independent mode", master->GetName().c_str(), master->GetGUIDLow());
        //return false;
    }

    //group member case (any player / any npcbot)
    if (gr && gr->IsMember(unit->GetGUID()))
        return true;

    //Player-controlled creature case
    if (Creature* cre = unit->ToCreature())
    {
        uint64 ownerGuid = unit->GetOwnerGUID();
        //controlled by group member
        if (gr && gr->IsMember(ownerGuid))
            return true;
        //controlled by playerbot
        Player* owner = ObjectAccessor::FindPlayer(ownerGuid);
        if (owner && owner->IsPlayerBot())
        {
            //master's playerbot
            if (PlayerbotMgrOwner* mgr = master->GetPlayerbotMgrOwner())
                if (mgr->GetPlayerBot(unit->GetGUID()))
                    return true;
            //other player's playerbot
            if (gr && gr->IsMember(owner->GetPlayerbotAI()->GetMaster()->GetGUID()))
                return true;
            //playerbot follower case
            if (gr && gr->IsMember(owner->GetPlayerbotAI()->GetFollowTargetGuid()))
                return true;
        }
    }
    //Player-controlled player case
    else if (Player* bot = unit->ToPlayer())
    {
        //Playerbot case
        if (bot->IsPlayerBot())
        {
            //master's playerbot
            if (PlayerbotMgrOwner* mgr = master->GetPlayerbotMgrOwner())
                if (mgr->GetPlayerBot(unit->GetGUID()))
                    return true;
            //other player's playerbot
            if (master->GetGroup() && master->GetGroup()->IsMember(bot->GetPlayerbotAI()->GetMaster()->GetGUID()))
                return true;
            //other player's playerbot follower case
            if (master->GetGroup() && master->GetGroup()->IsMember(bot->GetPlayerbotAI()->GetFollowTargetGuid()))
                return true;
        }
    }

    return false;
}

inline void PlayerbotAI::TellMaster(std::string const& text)
{
    Tell(text, _ownerGUID);
}

inline void PlayerbotAI::Tell(std::string const& text, uint64 targetGUID)
{
    if (text.empty() || !targetGUID || !IS_PLAYER_GUID(targetGUID))
        return;

    me->Whisper(text, LANG_UNIVERSAL, targetGUID);
}

Player* PlayerbotAI::GetMaster() const
{
    return HasBotState(BOTSTATE_INDEPENDENT) ? me : _mgr->GetMaster();
}

inline Spell* PlayerbotAI::GetCurrentSpell() const
{
    for (uint8 i = CURRENT_FIRST_NON_MELEE_SPELL; i != CURRENT_AUTOREPEAT_SPELL+1; ++i)
        if (Spell* spell = me->GetCurrentSpell(CurrentSpellTypes(i)))
            return spell;

    return NULL;
}

inline uint32 PlayerbotAI::GetCurrentSpellId() const
{
    Spell* spell = GetCurrentSpell();
    return spell ? spell->GetSpellInfo()->Id : 0;
}

inline bool PlayerbotAI::CCed(Unit* unit, bool root)
{
    return unit ? unit->HasUnitState(UNIT_STATE_CONFUSED | UNIT_STATE_STUNNED | UNIT_STATE_FLEEING | UNIT_STATE_DISTRACTED | UNIT_STATE_CONFUSED_MOVE | UNIT_STATE_FLEEING_MOVE) || (root && unit->HasUnitState(UNIT_STATE_ROOT)) : true;
}
uint32 PlayerbotAI::GetLostHP(Unit* unit)
{
    return unit->GetMaxHealth() - unit->GetHealth();
}

uint8 PlayerbotAI::GetHealthPCT(Unit* unit)
{
    return (!unit || unit->isDead()) ? 100 : (unit->GetHealth()*100/unit->GetMaxHealth());
}

uint8 PlayerbotAI::GetManaPCT(Unit* unit)
{
    return (!unit || unit->isDead() || unit->getPowerType() != POWER_MANA) ? 100 : (unit->GetPower(POWER_MANA)*100/unit->GetMaxPower(POWER_MANA));
}

void PlayerbotAI::SetFollowTarget(uint64 targetGUID)
{
    ASSERT(targetGUID != 0);

    if (_attackTargetGUID != 0 || me->duel)
    {
        TellMaster("I got a follow call while still in combat action");
        return;
    }

    if (_followTargetGUID != targetGUID)
    {
        me->ClearUnitState(UNIT_STATE_FOLLOW);
        if (IS_PLAYER_GUID(_followTargetGUID))
            if (Player* oldAcceptor = sObjectAccessor->FindPlayer(_followTargetGUID))
                oldAcceptor->GetPlayerbotMgrAcceptor()->RemoveBotFromMap(me->GetGUID());
    }

    std::ostringstream str;

    //botstate should be set before follow target anyways
    if (targetGUID == me->GetGUID() || targetGUID == _ownerGUID)
    {
        if (targetGUID == me->GetGUID())
        {
            ASSERT(HasBotState(BOTSTATE_INDEPENDENT));
            if (_followTargetGUID == me->GetGUID())
                TellMaster("Hey, I am already in free roam");
            else
                TellMaster("Entering free roam");
        }
        else if (targetGUID == _ownerGUID)
        {
            ASSERT(!HasBotState(BOTSTATE_INDEPENDENT));
            if (_followTargetGUID == _ownerGUID)
                TellMaster("I am already following you!");
            else
                TellMaster("Following you");
        }

        _followTargetGUID = targetGUID;
        return;
    }

    if (Unit* newTarget = sObjectAccessor->FindUnit(targetGUID))
    {
        std::ostringstream str;
        if (targetGUID == _followTargetGUID)
            str << "Already following " << newTarget->GetName() << '.';
        else
        {
            Tell("I am gonna go with you. I hope you are not against it because I will do it anyway.", targetGUID);
            str << "Following " << newTarget->GetName() << '!';
        }
        TellMaster(str.str());

        if (Player* acceptor = newTarget->ToPlayer())
        {
            PlayerbotMgrAcceptor* mgr = acceptor->GetPlayerbotMgrAcceptor();
            if (!mgr)
            {
                mgr = new PlayerbotMgrAcceptor(acceptor);
                acceptor->SetPlayerbotMgrAcceptor(mgr);
            }
            mgr->AcceptPlayerBot(me);
        }

        _followTargetGUID = targetGUID;
        return;
    }

    TellMaster("Cannot find my target! Gonna follow you then...");
    _followTargetGUID = _ownerGUID;
}

bool PlayerbotAI::IsUnitAttacking(Unit* attacker, Unit* victim)
{
    if (attacker->getVictim() == victim)
        return true;

    /*if (attacker->GetTypeId() == TYPEID_PLAYER)
    {
        if (attacker->ToPlayer()->GetSelection() != victim->GetGUID())
            return false
    }
    else */if (attacker->GetUInt64Value(UNIT_FIELD_TARGET) != victim->GetGUID())
        return false;

    if (attacker->HasUnitState(UNIT_STATE_CASTING))
    {
        Spell* spell = NULL;
        for (uint8 i = CURRENT_FIRST_NON_MELEE_SPELL; i != CURRENT_AUTOREPEAT_SPELL+1; ++i)
        {
            spell = attacker->GetCurrentSpell(CurrentSpellTypes(i));
            if (spell)
                break;
        }
        if (!spell)
            return false;

        //cannot cast positive spells on unfriendly and lower
        if (attacker->GetReactionTo(victim) < REP_NEUTRAL)
            return true;

        //last check (not precise)
        if (spell->GetSpellInfo()->Attributes & SPELL_ATTR0_NEGATIVE_1)
            return true;
    }

    return false;
}

inline float PlayerbotAI::GetMinAttackRange(Unit* target, bool ranged)
{
    //TODO: range for ranged classes
    return ranged ? 19.f : target->GetMeleeReach() * 0.8f;
}

bool PlayerbotAI::IsInFreeRoamMode() const
{
    return HasBotState(BOTSTATE_INDEPENDENT) && _followTargetGUID == me->GetGUID();
}

void PlayerbotAI::ApplyCustomMods(BotCustomMods mod, int &/*value*/) const
{
    switch (mod)
    {
        case BOT_CUSTOM_NONE:
            break;
        case BOT_CUSTOM_CASTTIME:
            break;
        default:
            break;
    }
}

void PlayerbotAI::Root() const
{
    me->StopMoving();
    if (!me->HasAura(SPELL_ROOT))
        me->AddAura(SPELL_ROOT, me);
}

void PlayerbotAI::UnRoot() const
{
    if (me->HasAura(SPELL_ROOT))
        me->RemoveAura(SPELL_ROOT);
}

bool PlayerbotAI::_MoveBehind(Unit* target) const
{
    ASSERT(target);
    if (me->HasUnitState(UNIT_STATE_ROOT))
        return false;

    if (!(_positioningFlags & POSITIONING_TANKING) &&
        !(_positioningFlags & POSITIONING_RANGED) &&
        target->IsWithinCombatRange(me, ATTACK_DISTANCE) &&
        target->HasInArc(M_PI, me) && 
        (me->getClass() == CLASS_ROGUE ? target->getVictim() != me || CCed(target) : target->getVictim() != me && !CCed(target)))
    {
        float x(0),y(0),z(0);
        target->GetNearPoint(me, x, y, z, me->GetObjectSize()/3, 0.1f, me->GetAngle(target));
        me->GetMotionMaster()->MovePoint(target->GetMapId(), x, y, z, false);
        return true;
    }

    return false;
}

void PlayerbotAI::_UpdateOutrunDistance()
{
    //base distance
    if (me->GetMap()->IsBattlegroundOrArena())
        _outrunDistance = sWorld->GetMaxVisibleDistanceInBGArenas();
    else if (me->GetMap()->Instanceable())
        _outrunDistance = sWorld->GetMaxVisibleDistanceInInstances();
    else
        _outrunDistance = sWorld->GetMaxVisibleDistanceOnContinents();

    //modifiers
    //1) mounts
    uint8 mountlvl = 1;
    if (!me->GetMap()->Instanceable() || sObjectMgr->GetInstanceTemplate(me->GetMap()->GetInstanceId())->AllowMount)
    {
        SpellInfo const* spellInfo;
        for (PlayerSpellMap::iterator itr = me->GetSpellMap().begin(); itr != me->GetSpellMap().end(); ++itr)
        {
            spellInfo = sSpellMgr->GetSpellInfo(itr->first);
            for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
            {
                if (spellInfo->Effects[i].ApplyAuraName == SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED)
                {
                    if (me->GetSkillValue(SKILL_RIDING) >= 225 && me->IsKnowHowFlyIn(me->GetMapId(), me->GetZoneId()))
                    {
                        if (spellInfo->CheckLocation(me->GetMapId(), me->GetZoneId(), me->GetAreaId(), me) == SPELL_CAST_OK)
                        {
                            mountlvl = 4;
                            break;
                        }
                    }
                }
                else if (spellInfo->Effects[i].ApplyAuraName == SPELL_AURA_MOD_INCREASE_MOUNTED_SPEED)
                {
                    if (spellInfo->Effects[i].BasePoints == 100 && me->GetSkillValue(SKILL_RIDING) >= 150)
                    {
                        if (spellInfo->CheckLocation(me->GetMapId(), me->GetZoneId(), me->GetAreaId(), me) == SPELL_CAST_OK)
                        {
                            if (mountlvl < 3)
                                mountlvl = 3;
                            break;
                        }
                    }
                    else if (spellInfo->Effects[i].BasePoints == 60 && me->GetSkillValue(SKILL_RIDING) >= 75)
                    {
                        if (spellInfo->CheckLocation(me->GetMapId(), me->GetZoneId(), me->GetAreaId(), me) == SPELL_CAST_OK)
                        {
                            if (mountlvl < 2)
                                mountlvl = 2;
                            break;
                        }
                    }
                } 
            }
            if (mountlvl >= 4)
                break;
        }
    }

    _outrunDistance *= mountlvl;
}

void PlayerbotAI::_UpdatePositioningFlags()
{
    _positioningFlags = 0;
    if (HasBotState(BOTSTATE_FOLLOW))
    {
        if (HasBotState(BOTSTATE_COMBAT))
        {
            sLog->outError(LOG_FILTER_PLAYER, "PAI::_UpdatePositioningFlags() Error! Bot %s (%u) has BOTSTATE_FOLLOW and BOTSTATE_COMBAT at same time! Removing combat state", me->GetName().c_str(), me->GetGUIDLow());
            ClearBotState(BOTSTATE_COMBAT);
        }
        _positioningFlags |= POSITIONING_FOLLOW;
    }

    if (HasBotState(BOTSTATE_LOOTING))
        _positioningFlags |= POSITIONING_LOOTING;

    if (HasBotState(BOTSTATE_COMBAT))
    {
        if (HasBotState(BOTSTATE_LOOTING))
        {
            sLog->outError(LOG_FILTER_PLAYER, "PAI::_UpdatePositioningFlags() Error! Bot %s (%u) has BOTSTATE_COMBAT and BOTSTATE_LOOTING at same time! Removing looting state", me->GetName().c_str(), me->GetGUIDLow());
            ClearBotState(BOTSTATE_LOOTING);
        }

        _positioningFlags |= POSITIONING_ATTACK;
    }

    if (_classAI->GetRoleMask() & ROLE_DAMAGE_MELEE)
        _positioningFlags |= POSITIONING_MELEE;

    if (_classAI->GetRoleMask() & ROLE_DAMAGE_RANGED)
        _positioningFlags |= POSITIONING_RANGED;

    if (_classAI->GetRoleMask() & ROLE_TANK)
    {
        if (_classAI->GetRoleMask() & ROLE_DAMAGE_MELEE)
        {
            sLog->outError(LOG_FILTER_PLAYER, "PAI::_UpdatePositioningFlags() Error! Bot %s (%u) has ROLE_DAMAGE_MELEE and ROLE_TANK at same time! Removing tanking", me->GetName().c_str(), me->GetGUIDLow());
            _classAI->ClearRole(ROLE_TANK);
        }
        else if (_classAI->GetRoleMask() & ROLE_DAMAGE_RANGED)
        {
            sLog->outError(LOG_FILTER_PLAYER, "PAI::_UpdatePositioningFlags() Error! Bot %s (%u) has ROLE_DAMAGE_RANGED and ROLE_TANK at same time! Removing tanking", me->GetName().c_str(), me->GetGUIDLow());
            _classAI->ClearRole(ROLE_TANK);
        }
        else
            _positioningFlags |= POSITIONING_TANKING;
    }

    if (!(_classAI->GetRoleMask() & (ROLE_TANK | ROLE_DAMAGE_MELEE | ROLE_DAMAGE_RANGED)))
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::_UpdatePositioningFlags() Error! Bot %s (%u) has no melee nor ranged nor tanking role is assigned! Set to melee", me->GetName().c_str(), me->GetGUIDLow());
        _classAI->AddRole(ROLE_DAMAGE_MELEE);
        if (HasBotState(BOTSTATE_COMBAT))
            _positioningFlags |= POSITIONING_MELEE_ATTACK;
        else
            _positioningFlags |= POSITIONING_MELEE;
    }
}
//HOOKS
void PlayerbotAI::OnLevelChange(uint8 oldlevel, uint8 newlevel)
{
    if (oldlevel == newlevel)
        return;
    _classAI->InitSpells();
}

void PlayerbotAI::OnStopMoving()
{
    if (_attackTargetGUID == 0)
        return;

    if (Unit* unit = sObjectAccessor->FindUnit(_attackTargetGUID))
    {
        me->SetOrientation(me->GetAngle(unit));
        me->SetFacingTo(me->GetAngle(unit));
    }

    _attackTargetGUID = 0;
}

void PlayerbotAI::OnZoneChange()
{
    _UpdateOutrunDistance();
}

void PlayerbotAI::OnSpellLearned(uint32 spellId)
{
    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId))
    {
        if (!spellInfo->IsPassive() || spellInfo->Id == 54197) //cold weather flying
        {
            _UpdateOutrunDistance();
        }
    }
    _classAI->InitSpells();
}

void PlayerbotAI::OnSpellRemoved(uint32 spellId)
{
    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId))
    {
        if (!spellInfo->IsPassive() || spellInfo->Id == 54197) //cold weather flying
        {
            _UpdateOutrunDistance();
        }
    }
    _classAI->InitSpells();
}

void PlayerbotAI::OnSkillSet(uint64 sourceGuid, uint16 skillId, uint16 /*step*/, uint16 /*newVal*/, uint16 /*maxVal*/)
{
    if (sourceGuid == me->GetGUID())
        if (skillId == SKILL_RIDING)
            _UpdateOutrunDistance();
}
void PlayerbotAI::OnDuelRequest(uint64 flagGuid, Player* requester)
{
    WorldPacket* packet;
    if (me->isMoving() || me->isInCombat() || me->GetMap()->Instanceable() || _attackTargetGUID != 0)
    {
        packet = new WorldPacket(CMSG_DUEL_CANCELLED, 8);
        *packet << uint64(me->GetGUID());
        me->GetSession()->QueuePacket(packet); //packet is not used
        return;
    }

    packet = new WorldPacket(CMSG_DUEL_ACCEPTED, 8);
    *packet << flagGuid;
    me->GetSession()->QueuePacket(packet);

    me->SetOrientation(me->GetAngle(requester));
    me->SetFacingTo(me->GetAngle(requester));
    me->SetSelection(requester->GetGUID());

    // follow target in casting range
    float angle = frand(0.f, M_PI/2.f);
    float dist = GetMinAttackRange(requester, (_positioningFlags & POSITIONING_RANGED));

    //AddPositioningFlag(POSITIONING_ATTACK);
    //_attackTargetGUID = requester->GetGUID();
    SetMovement(0, requester, _positioningFlags, dist, angle);
}

void PlayerbotAI::OnDuelStart()
{
    _attackTargetGUID = me->duel->opponent->GetGUID();
    AddPositioningFlag(POSITIONING_ATTACK);
    AddCombatState(COMBAT_STATE_DUEL);
    //COMBAT_STATE_PVP is added in HasAttackTarget()
    if (!HasAttackTarget(_classAI->GetDamageSchoolMask()))
    {
        sLog->outError(LOG_FILTER_PLAYER, "PAI::OnDuelStart() Bot %s (%u) has no target! Resetting", me->GetName().c_str(), me->GetGUIDLow());
        ClearPositioningFlag(POSITIONING_ATTACK);
        ClearCombatState(COMBAT_STATE_DUEL);
        return;
    }
}

void PlayerbotAI::OnDuelComplete(uint8 /*type*/)
{
    me->SetSelection(0);

    _attackTargetGUID = 0;
    ClearBotState(BOTSTATE_COMBAT);
    ClearCombatState(COMBAT_STATE_DUEL);
    ClearCombatState(COMBAT_STATE_PVP);
    ClearPositioningFlag(POSITIONING_ATTACK);
    me->StopMoving();
    AddBotState(BOTSTATE_FOLLOW);
}

void PlayerbotAI::OnGroupLeaderChange(std::string name)
{
    Group* gr = me->GetGroup();
    ASSERT(gr);

    if (!name.compare(me->GetName()))
    {
        if (_followTargetGUID != me->GetGUID())
        {
            if (Unit* target = sObjectAccessor->FindUnit(_followTargetGUID))
                if (Player* owner = target->GetCharmerOrOwnerPlayerOrPlayerItself())
                    if (gr->IsMember(owner->GetGUID()))
                    {
                        WorldPacket* packet = new WorldPacket(CMSG_GROUP_SET_LEADER, 8);
                        *packet << uint64(owner->GetGUID());
                        me->GetSession()->QueuePacket(packet);
                        return;
                    }
        }

        for (GroupReference const* ref = gr->GetFirstMember(); ref != NULL; ref = ref->next())
        {
            Player* pl = ref->getSource();
            if (!pl || !pl->IsInWorld() || pl == me || pl->IsPlayerBot())
                continue;

            WorldPacket* packet = new WorldPacket(CMSG_GROUP_SET_LEADER, 8);
            *packet << uint64(pl->GetGUID());
            me->GetSession()->QueuePacket(packet);
            break;
        }
    }
}

void PlayerbotAI::OnGroupInvite()
{
    if (me->GetGroup())
        return;

    Group* group = me->GetGroupInvite();
    if (!group)
        return;

    uint64 leaderGUID = group->GetLeaderGUID();
    Player* inviter = sObjectAccessor->FindPlayer(leaderGUID);
    if (!inviter)
        return;

    //Decide if we should accept this invite
    //Check for any tie with this group
    bool accept;

    Player* owner = me->GetSession()->m_master;
    if (inviter == owner)
        accept = true;
    else if (owner->GetSocial()->HasIgnore(inviter->GetGUIDLow()))
        accept = false;
    else if (owner->GetSocial()->HasFriend(inviter->GetGUIDLow()))
        accept = true;
    else if (WorldObject* followTarget = ObjectAccessor::GetWorldObject(*me, _followTargetGUID))
    {
        uint64 followPlayerGUID = 0;
        if (followTarget->GetTypeId() == TYPEID_PLAYER)
            followPlayerGUID = _followTargetGUID;
        else if (followTarget->GetTypeId() == TYPEID_CORPSE)
            followPlayerGUID = followTarget->ToCorpse()->GetOwnerGUID();
        else if (followTarget->GetTypeId() == TYPEID_UNIT)
            followPlayerGUID = followTarget->ToCreature()->GetOwnerGUID();

        accept = group->IsCreated() ? group->IsMember(followPlayerGUID) : followPlayerGUID == leaderGUID;
    }
    else
        accept = (owner->GetGroup() == group);

    if (accept == false)
    {
        WorldPacket* packet = new WorldPacket(CMSG_GROUP_DECLINE, 8);
        *packet << me->GetGUID();
        Tell("I will not join your group, you are not a friend to my master nor me", leaderGUID);
        //me->GetSession()->HandleGroupDeclineOpcode(packet); //packet not used
        me->GetSession()->QueuePacket(packet);
    }
    else
    {
        WorldPacket* packet = new WorldPacket(CMSG_GROUP_ACCEPT, 4);
        *packet << uint32(0);
        //me->GetSession()->HandleGroupAcceptOpcode(packet);
        me->GetSession()->QueuePacket(packet);
    }
}

void PlayerbotAI::OnPartyResult(uint8 operation, std::string const& member, PartyResult res, uint32 /*val*/)
{
    //not succeed, fake call
    if (res != ERR_PARTY_RESULT_OK)
        return;

    PartyOperation op = PartyOperation(operation);
    switch (op)
    {
        case PARTY_OP_INVITE:
            return OnGroupInvite();

        case PARTY_OP_LEAVE:
        {
            WorldPacket* packet = new WorldPacket(CMSG_GROUP_DISBAND, 8);
            *packet << me->GetGUID();
            if (_followTargetGUID == _ownerGUID)
            {
                if (GetMaster()->GetGroup() == me->GetGroup())
                {
                    me->GetSession()->QueuePacket(packet); //not used
                    break;
                }
            }
            else //independent
            {
                Player* accepter = sObjectAccessor->FindPlayer(_followTargetGUID);
                if (accepter && accepter->GetName() == member)
                {
                    me->GetSession()->QueuePacket(packet);
                    break;
                }
            }

            //didn't hit
            delete packet;
            packet = NULL;

            break;
        }
    }
}

void PlayerbotAI::OnResurrectRequest(uint64 casterGuid)
{
    WorldPacket* packet = new WorldPacket(CMSG_RESURRECT_RESPONSE, 8 + 1);
    *packet << uint64(casterGuid);
    *packet << uint8(1); //status (0 = rejected)
    me->GetSession()->QueuePacket(packet);
}

void PlayerbotAI::OnReceiveTextEmote(Player* player, uint32 text_emote)
{
    if (player->GetGUID() != _ownerGUID/* && player->GetGUID() != _followTargetGUID*/)
        return;

    switch (text_emote)
    {
        case TEXT_EMOTE_BONK:
        {
            SendDebugInfo();
            break;
        }
        default:
            break;
    }
}

void PlayerbotAI::OnBuyError(uint64 /*vendorGuid*/, uint32 itemId, uint32 msg)
{
    //no one to report
    if (_followTargetGUID == 0 || _followTargetGUID == me->GetGUID() || !IS_PLAYER_GUID(_followTargetGUID))
        return;

    ItemTemplate const* item = sObjectMgr->GetItemTemplate(itemId);
    if (!item)
        return;

    Player* player = sObjectAccessor->FindPlayer(_followTargetGUID);
    if (!player || !player->IsInWorld())
        return;

    std::ostringstream str;
    switch (msg)
    {
        case BUY_ERR_NOT_ENOUGHT_MONEY:
            str << "I can't buy ";
            _AddItemTemplateLink(player, item, str);
            str << ". Can't afford it";
            Tell(str.str(), _followTargetGUID);
            break;
        case BUY_ERR_CANT_CARRY_MORE:
            TellMaster("I've just received BUY_ERR_CANT_CARRY_MORE! This should not ever happen");
            break;
        case BUY_ERR_CANT_FIND_ITEM:
        case BUY_ERR_ITEM_ALREADY_SOLD:
        case BUY_ERR_SELLER_DONT_LIKE_YOU:
        case BUY_ERR_DISTANCE_TOO_FAR:
        case BUY_ERR_ITEM_SOLD_OUT:
        case BUY_ERR_RANK_REQUIRE:
        case BUY_ERR_REPUTATION_REQUIRE:
        default:
            break;
    }
}

void PlayerbotAI::OnEquipError(uint32 msg, Item const* item1, Item const* /*item2*/)
{
    //no one to report
    if (_followTargetGUID == 0 || _followTargetGUID == me->GetGUID() || !IS_PLAYER_GUID(_followTargetGUID))
        return;

    Player* player = sObjectAccessor->FindPlayer(_followTargetGUID);
    if (!player || !player->IsInWorld())
        return;

    std::ostringstream str;
    if (msg == EQUIP_ERR_OK)
    {
        //str << "received "
        //_AddItemTemplateLink(player, item1, str);
        //Tell(str.str(), _followTargetGUID);
        return;
    }

    if (!item1)
    {
        str << "Unknown item in OnEquipError(), msg = " << uint32(msg);
        Tell(str.str(), _followTargetGUID);
        return;
    }

    switch (msg)
    {
        case EQUIP_ERR_CANT_CARRY_MORE_OF_THIS:
            str << "I can't carry any more of ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_MISSING_REAGENT:
            str << "I'm missing some reagents for ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_ITEM_LOCKED:
            str << "This ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << " is locked! Not sure if I can open it...";
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_ALREADY_LOOTED:
            str << "This ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << " is already looted! Jerks!";
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_INVENTORY_FULL:
            str << "It must be something wrong but I have no free space for ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_NOT_IN_COMBAT:
            str << "Cannot use ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << ", this item canot be used in combat!";
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_LOOT_CANT_LOOT_THAT_NOW:
            str << "Hmm, cannot loot ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << " right now...";
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_ITEM_UNIQUE_EQUIPABLE:
            str << "I cannot equip another ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << ", alredy have one";
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_BANK_FULL:
            str << "My bank is full, cannot store this ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_ITEM_NOT_FOUND:
            str << "Cannot find ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << '!';
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_TOO_FAR_AWAY_FROM_BANK:
            str << "I am too far away from bank to store ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << '!';
            Tell(str.str(), _followTargetGUID);
            break;
        case EQUIP_ERR_NONE:
            str << "Cannot use this ";
            _AddItemTemplateLink(player, item1->GetTemplate(), str);
            str << "right now";
            Tell(str.str(), _followTargetGUID);
            break;
    }
}

void PlayerbotAI::OnSpellCastFail(SpellInfo const* spellInfo, uint32 result, uint32 /*customError*/)
{
    //no one to report
    if (_followTargetGUID == 0 || _followTargetGUID == me->GetGUID() || !IS_PLAYER_GUID(_followTargetGUID))
        return;

    Player* player = sObjectAccessor->FindPlayer(_followTargetGUID);
    if (!player || !player->IsInWorld())
        return;

    std::ostringstream str;
    str << "My ";
    _AddSpellLink(player, spellInfo, str);
    str << " failed. ";
    switch (result)
    {
        case SPELL_FAILED_INTERRUPTED:
            //Reaction on this should be called in Unit class
            str << "Interrupted somehow";
            break;
        case SPELL_FAILED_UNIT_NOT_INFRONT:
            str << "Target is not in front";
            break;
        case SPELL_FAILED_BAD_TARGETS:
            str << "Bad target";
            break;
        case SPELL_FAILED_REQUIRES_SPELL_FOCUS:
            switch (spellInfo->RequiresSpellFocus) //SpellFocusObject.dbc id
            {
                case 1: //Anvil
                    str << "|cffff0000Requires anvil.";
                    break;
                case 2:  //Loom
                    str << "|cffff0000Requires mana loom.";
                    break;
                case 3:  //Forge
                    str << "|cffff0000Requires forge.";
                    break;
                case 4:  //Cooking Fire
                    str << "|cffff0000Requires cooking fire.";
                    break;
                default:
                    str << "|cffff0000Requires Spell Focus with id " << uint32(spellInfo->RequiresSpellFocus);
                    break;
            }
            break;
        case SPELL_FAILED_CANT_BE_DISENCHANTED:
            str << "|cffff0000Item cannot be disenchanted.";
            break;
        case SPELL_FAILED_CANT_BE_MILLED:
            str << "|cffff0000Item cannot be milled.";
            break;
        case SPELL_FAILED_CANT_BE_PROSPECTED:
            str << "|cffff0000Item cannot be prospected.";
            break;
        case SPELL_FAILED_EQUIPPED_ITEM_CLASS:
            str << "|cffff0000This item is not a valid for this spell.";
            break;
        case SPELL_FAILED_NEED_MORE_ITEMS:
            str << "|cffff0000Targeted items stack is too small.";
            break;
        case SPELL_FAILED_REAGENTS:
            str << "|cffff0000Need reagents";
            break;
        default:
            str << "|cff000000Unhandled reason: " << uint32(result);
            break;
    }
    Tell(str.str(), _followTargetGUID);
}

void PlayerbotAI::OnSendTradeStatus(uint32 status)
{
    //TODO
    Player* trader = me->GetTrader();
    ASSERT(trader);

    WorldPacket* packet = NULL;
    switch (status)
    {
        case TRADE_STATUS_BEGIN_TRADE:
            packet = new WorldPacket(CMSG_BEGIN_TRADE, 8);
            *packet << me->GetGUID();
            me->GetSession()->QueuePacket(packet); // packet not used

            if (trader->GetGUID() != _followTargetGUID)
            {
                // TODO: Really? What if I give a bot all my junk so it's inventory is full when a nice green/blue/purple comes along?
                Tell("I'm not allowed to trade you any of my items, but you are free to give me money or items.", trader->GetGUID());
                return;
            }

            //// list out items available for trade
            //std::ostringstream out;
            //std::list<std::string> lsItemsTradable;
            //std::list<std::string> lsItemsUntradable;

            //// list out items in main backpack
            //for (uint8 slot = INVENTORY_SLOT_ITEM_START; slot < INVENTORY_SLOT_ITEM_END; slot++)
            //{
            //    const Item* const pItem = m_bot->GetItemByPos(INVENTORY_SLOT_BAG_0, slot);
            //    if (pItem)
            //    {
            //        MakeItemLink(pItem, out, true);
            //        if (pItem->CanBeTraded())
            //            lsItemsTradable.push_back(out.str());
            //        else
            //            lsItemsUntradable.push_back(out.str());
            //        out.str("");
            //    }
            //}

            //// list out items in other removable backpacks
            //for (uint8 bag = INVENTORY_SLOT_BAG_START; bag < INVENTORY_SLOT_BAG_END; ++bag)
            //{
            //    const Bag* const pBag = (Bag *) m_bot->GetItemByPos(INVENTORY_SLOT_BAG_0, bag);
            //    if (pBag)
            //        for (uint8 slot = 0; slot < pBag->GetBagSize(); ++slot)
            //        {
            //            const Item* const pItem = m_bot->GetItemByPos(bag, slot);
            //            if (pItem)
            //            {
            //                MakeItemLink(pItem, out, true);
            //                if (pItem->CanBeTraded())
            //                    lsItemsTradable.push_back(out.str());
            //                else
            //                    lsItemsUntradable.push_back(out.str());
            //                out.str("");
            //            }
            //        }
            //}

            //ChatHandler ch(m_bot->GetTrader());
            //out.str("");
            //out << "Items I have but cannot trade:";
            //uint32 count = 0;
            //for (std::list<std::string>::iterator iter = lsItemsUntradable.begin(); iter != lsItemsUntradable.end(); iter++)
            //{
            //    out << (*iter);
            //    // Why this roundabout way of posting max 20 items per whisper? To keep the list scrollable.
            //    count++;
            //    if (count % 20 == 0)
            //    {
            //        ch.SendSysMessage(out.str().c_str());
            //        out.str("");
            //    }
            //}
            //if (count > 0)
            //    ch.SendSysMessage(out.str().c_str());

            //out.str("");
            //out << "I could give you:";
            //count = 0;
            //for (std::list<std::string>::iterator iter = lsItemsTradable.begin(); iter != lsItemsTradable.end(); iter++)
            //{
            //    out << (*iter);
            //    // Why this roundabout way of posting max 20 items per whisper? To keep the list scrollable.
            //    count++;
            //    if (count % 20 == 0)
            //    {
            //        ch.SendSysMessage(out.str().c_str());
            //        out.str("");
            //    }
            //}
            //if (count > 0)
            //    ch.SendSysMessage(out.str().c_str());
            //else
            //    ch.SendSysMessage("I have nothing to give you.");

            //// calculate how much money bot has
            //// send bot the message
            //uint32 copper = m_bot->GetMoney();
            //out.str("");
            //out << "I have |cff00ff00" << Cash(copper) << "|r";
            //SendWhisper(out.str().c_str(), *(m_bot->GetTrader()));
            break;
        case TRADE_STATUS_TRADE_ACCEPT:
            me->GetSession()->HandleAcceptTradeOpcode(*packet); //not used
            //SetQuestNeedItems();
            //AutoUpgradeEquipment();
            break;
        default:
            break;
    }
}

void PlayerbotAI::OnSpellGo(SpellInfo const* /*spellInfo*/, uint32 /*castFlags*/)
{
    //SPELL LAUNCHED
    //TODO CLASS-SPECIFIED EVENTS
    //_classAI->OnSpellGo(spellInfo, castFlags);
}

void PlayerbotAI::OnSpellHit(Unit* /*attacker*/, SpellInfo const* /*spellInfo*/)
{
    //HIT BY SPELL
    //TODO CLASS-SPECIFIED EVENTS
    //_classAI-OnSpellHit(attacker, spellInfo);
}
void PlayerbotAI::OnSpellHitTarget(Player* /*caster*/, Unit* /*victim*/, SpellInfo const* /*spellInfo*/)
{
    //ENEMY HIT BY OUR SPELL
    //TODO CLASS-SPECIFIED EVENTS
    //_classAI-OnSpellHitTarget(caster, victim, spellInfo);
}

void PlayerbotAI::OnLootWindowOpen(Loot* /*loot*/, uint32 /*persmission*/)
{
    //uint64 lootguid = me->GetLootGUID(); // != NULL
    //TODO:
    //Handling loot and related
    //Also Rolls
}

void PlayerbotAI::OnLootWindowClose()
{
    //uint64 lootguid = me->GetLootGUID(); // != NULL
    //TODO:
    //Handling after looting actions
}

void PlayerbotAI::OnLootRollWon(uint64 /*lootTargetGuid*/, uint32 /*item*/)
{
    //TODO:
    //Erase target from loot list
    //Release loot
}

void PlayerbotAI::OnPartyKill(uint64 /*killerGuid*/, uint64 /*victimGuid*/, uint64 /*looterGuid*/)
{
    ASSERT(me->GetGroup());
    //TODO:
    //Add target to loot list (if creature)
    //Class-specified
    //updatequests
}

void PlayerbotAI::OnKill(uint64 /*victimGuid*/)
{
    //ASSERT(me->GetGroup() || !me->GetGroup())
    //TODO:
    //Add target to loot list (if creature and no group)
    //Class-specified (if victim is player)
    //updatequests
}

void PlayerbotAI::OnItemReceive(Item* /*item*/, uint32 /*count*/, uint32 /*totalcount*/, uint8 /*bagslot*/, uint8 /*itemslot*/, bool /*received*/, bool /*created*/)
{
    //TODO:
    //updatequests
    //if (itemisusefulandcanequip)
    //    equipitem
    //    storeuneqippeditemifany
    //if (storeiteminvirtualbackpack)
    //    removefromrealbag
    //tellmaster
}
//UTILITIES
void PlayerbotAI::_AddItemTemplateLink(Player* forPlayer, ItemTemplate const* item, std::ostringstream &str) const
{
    //color
    str << "|c";
    switch (item->Quality)
    {
        case ITEM_QUALITY_POOR:     str << "ff9d9d9d"; break;  //GREY
        case ITEM_QUALITY_NORMAL:   str << "ffffffff"; break;  //WHITE
        case ITEM_QUALITY_UNCOMMON: str << "ff1eff00"; break;  //GREEN
        case ITEM_QUALITY_RARE:     str << "ff0070dd"; break;  //BLUE
        case ITEM_QUALITY_EPIC:     str << "ffa335ee"; break;  //PURPLE
        case ITEM_QUALITY_LEGENDARY:str << "ffff8000"; break;  //ORANGE
        case ITEM_QUALITY_ARTIFACT: str << "ffe6cc80"; break;  //LIGHT YELLOW
        case ITEM_QUALITY_HEIRLOOM: str << "ffe6cc80"; break;  //LIGHT YELLOW
        default:                    str << "ff000000"; break;  //UNK BLACK
    }
    str << "|Hitem:" << uint32(item->ItemId) << ":";

    //permanent enchantment, 3 gems, 4 unknowns, reporter_level (9)
    str << "0:0:0:0:0:0:0:0:0";

    //name
    std::string name = item->Name1;
    _LocalizeItem(forPlayer, name, item->ItemId);
    str << "|h[" << name << "]|h|r";

    //max in stack
    if (item->BuyCount > 1)
        str<< "|cff009900x" << item->BuyCount << ".|r";
    else
        str << "|cff009900.|r";
}

void PlayerbotAI::_AddItemLink(Player* forPlayer, Item const* item, std::ostringstream &str) const
{
    ItemTemplate const* proto = item->GetTemplate();

    //color
    str << "|c";
    switch (proto->Quality)
    {
        case ITEM_QUALITY_POOR:     str << "ff9d9d9d"; break;  //GREY
        case ITEM_QUALITY_NORMAL:   str << "ffffffff"; break;  //WHITE
        case ITEM_QUALITY_UNCOMMON: str << "ff1eff00"; break;  //GREEN
        case ITEM_QUALITY_RARE:     str << "ff0070dd"; break;  //BLUE
        case ITEM_QUALITY_EPIC:     str << "ffa335ee"; break;  //PURPLE
        case ITEM_QUALITY_LEGENDARY:str << "ffff8000"; break;  //ORANGE
        case ITEM_QUALITY_ARTIFACT: str << "ffe6cc80"; break;  //LIGHT YELLOW
        case ITEM_QUALITY_HEIRLOOM: str << "ffe6cc80"; break;  //LIGHT YELLOW
        default:                    str << "ff000000"; break;  //UNK BLACK
    }
    str << "|Hitem:" << proto->ItemId << ":";

    //permanent enchantment
    str << item->GetEnchantmentId(PERM_ENCHANTMENT_SLOT) << ":";

    //gems (3)
    uint32 g1 = 0, g2 = 0, g3 = 0;
    for (uint32 slot = SOCK_ENCHANTMENT_SLOT; slot != SOCK_ENCHANTMENT_SLOT + MAX_GEM_SOCKETS; ++slot)
    {
        uint32 eId = item->GetEnchantmentId(EnchantmentSlot(slot));
        if (!eId)
            continue;

        SpellItemEnchantmentEntry const* enchant = sSpellItemEnchantmentStore.LookupEntry(eId);
        if (!enchant) continue;

        switch (slot - SOCK_ENCHANTMENT_SLOT)
        {
            case 1: g1 = enchant->GemID;    break;
            case 2: g2 = enchant->GemID;    break;
            case 3: g3 = enchant->GemID;    break;
        }
    }
    str << g1 << ":" << g2 << ":" << g3 << ":";

    //temp enchantment, bonus enchantment and prismatic enchantment (3 + 1 unk)
    str << "0:0:0:0:";

    //reporter level
    str << "0";

    //name
    std::string name = proto->Name1;
    _LocalizeItem(forPlayer, name, proto->ItemId);
    str << "|h[" << name << "]|h|r";

    //quantity
    if (item->GetCount() > 1)
        str << "x" << item->GetCount() << ' ';
}

void PlayerbotAI::_AddQuestLink(Player* forPlayer, Quest const* quest, std::ostringstream &str) const
{
    std::string questTitle = quest->GetTitle();
    _LocalizeQuest(forPlayer, questTitle, quest->GetQuestId());
    str << "|cFFEFFD00|Hquest:" << quest->GetQuestId() << ':' << quest->GetQuestLevel() << "|h[" << questTitle << "]|h|r";
}

void PlayerbotAI::_AddWeaponSkillLink(Player* forPlayer, SpellInfo const* spellInfo, std::ostringstream &str, uint32 skillid) const
{
    uint32 loc = forPlayer->GetSession()->GetSessionDbcLocale();
    str << "|cff00ffff|Hspell:" << spellInfo->Id << "|h[" << spellInfo->SpellName[loc] << " : " << me->GetSkillValue(skillid) << " /" << me->GetMaxSkillValue(skillid) << "]|h|r";
}

void PlayerbotAI::_AddSpellLink(Player* forPlayer, SpellInfo const* spellInfo, std::ostringstream &str) const
{
    uint32 loc = forPlayer->GetSession()->GetSessionDbcLocale();
    str << "|cffffffff|Hspell:" << spellInfo->Id << "|h[" << spellInfo->SpellName[loc] << "]|h|r";
}

void PlayerbotAI::_AddProfessionLink(Player* forPlayer, SpellInfo const* spellInfo, std::ostringstream &str, uint32 skillId) const
{
    ASSERT(me->HasSkill(skillId));
    // |cffffd000|Htrade:4037:1:150:1:6AAAAAAAAAAAAAAAAAAAAAAOAADAAAAAAAAAAAAAAAAIAAAAAAAAA|h[Engineering]|h|r
    uint32 loc = forPlayer->GetSession()->GetSessionDbcLocale();
    SkillLineEntry const* skillInfo = sSkillLineStore.LookupEntry(skillId);
    if (skillInfo)
    {
        uint32 curValue = me->GetPureSkillValue(skillId);
        uint32 maxValue  = me->GetPureMaxSkillValue(skillId);
        str << "|cffffd000|Htrade:" << spellInfo->Id << ':' << curValue << ':' << maxValue << ':' << me->GetGUIDLow() << ":6AAAAAAAAAAAAAAAAAAAAAAOAADAAAAAAAAAAAAAAAAIAAAAAAAAA" << "|h[" << skillInfo->name[loc] << "]|h|r";
    }
}
//Localization
void PlayerbotAI::_LocalizeItem(Player* forPlayer, std::string &itemName, uint32 entry) const
{
    uint32 loc = forPlayer->GetSession()->GetSessionDbLocaleIndex();
    std::wstring wnamepart;

    ItemLocale const* itemInfo = sObjectMgr->GetItemLocale(entry);
    if (!itemInfo)
        return;

    if (itemInfo->Name.size() > loc && !itemInfo->Name[loc].empty())
    {
        const std::string name = itemInfo->Name[loc];
        if (Utf8FitTo(name, wnamepart))
            itemName = name;
    }
}

void PlayerbotAI::_LocalizeQuest(Player* forPlayer, std::string &questTitle, uint32 entry) const
{
    uint32 loc = forPlayer->GetSession()->GetSessionDbLocaleIndex();
    std::wstring wnamepart;

    QuestLocale const* questInfo = sObjectMgr->GetQuestLocale(entry);
    if (!questInfo)
        return;

    if (questInfo->Title.size() > loc && !questInfo->Title[loc].empty())
    {
        const std::string title = questInfo->Title[loc];
        if (Utf8FitTo(title, wnamepart))
            questTitle = title;
    }
}

void PlayerbotAI::_LocalizeCreature(Player* forPlayer, std::string &creatureName, uint32 entry) const
{
    uint32 loc = forPlayer->GetSession()->GetSessionDbLocaleIndex();
    std::wstring wnamepart;

    CreatureLocale const* creatureInfo = sObjectMgr->GetCreatureLocale(entry);
    if (!creatureInfo)
        return;

    if (creatureInfo->Name.size() > loc && !creatureInfo->Name[loc].empty())
    {
        const std::string title = creatureInfo->Name[loc];
        if (Utf8FitTo(title, wnamepart))
            creatureName = title;
    }
}

void PlayerbotAI::_LocalizeGameObject(Player* forPlayer, std::string &gameobjectName, uint32 entry) const
{
    uint32 loc = forPlayer->GetSession()->GetSessionDbLocaleIndex();
    std::wstring wnamepart;

    GameObjectLocale const* gameObjectInfo = sObjectMgr->GetGameObjectLocale(entry);
    if (!gameObjectInfo)
        return;

    if (gameObjectInfo->Name.size() > loc && !gameObjectInfo->Name[loc].empty())
    {
        const std::string title = gameObjectInfo->Name[loc];
        if (Utf8FitTo(title, wnamepart))
            gameobjectName = title;
    }
}

void PlayerbotAI::_UnbindInstance(uint32 mapId, Difficulty diff)
{
    for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
    {
        Player::BoundInstancesMap& binds = me->GetBoundInstances(Difficulty(i));
        for (Player::BoundInstancesMap::iterator itr = binds.begin(); itr != binds.end();)
        {
            InstanceSave* save = itr->second.save;
            if (itr->first != me->GetMapId() && mapId == itr->first && diff == save->GetDifficulty())
            {
                ChatHandler(GetMaster()->GetSession()).PSendSysMessage("PlayerbotAI: unbinding %s's map: %d inst: %d perm: %s diff: %d canReset: %s", me->GetName().c_str(), itr->first, save->GetInstanceId(), itr->second.perm ? "yes" : "no", save->GetDifficulty(), save->CanReset() ? "yes" : "no");
                me->UnbindInstance(itr, Difficulty(i));
                break;
            }
            else
                ++itr;
        }
    }
}

void PlayerbotAI::_SendDebugInfo()
{
    //uint8 loc = uint8(me->GetSession()->GetSessionDbcLocale());
    ChatHandler ch(me->GetSession()->m_master->GetSession());

    ch.PSendSysMessage("Listing stats for %s:", me->GetName().c_str());
    std::ostringstream str1;
    str1 << "1) Botstates: (";
    if (HasBotState(BOTSTATE_FOLLOW))
        str1 << "BOTSTATE_FOLLOW";
    if (HasBotState(BOTSTATE_STAY))
        str1 << " | BOTSTATE_STAY";
    if (HasBotState(BOTSTATE_COMBAT))
        str1 << " | BOTSTATE_COMBAT";
    if (HasBotState(BOTSTATE_LOOTING))
        str1 << " | BOTSTATE_LOOTING";
    if (HasBotState(BOTSTATE_TAME))
        str1 << " | BOTSTATE_TAME";
    if (HasBotState(BOTSTATE_DELAYED))
        str1 << " | BOTSTATE_DELAYED";
    if (HasBotState(BOTSTATE_RESET))
        str1 << " | BOTSTATE_RESET";
    if (HasBotState(BOTSTATE_INDEPENDENT))
        str1 << " | BOTSTATE_INDEPENDENT";
    str1 << ')';
    ch.PSendSysMessage(str1.str().c_str());

    std::ostringstream str2;
    str2 << "2) Combatstates: (";
    if (HasCombatState(COMBAT_STATE_RANGED))
        str2 << " | COMBAT_STATE_RANGED";
    if (HasCombatState(COMBAT_STATE_STAY))
        str2 << " | COMBAT_STATE_STAY";
    if (HasCombatState(COMBAT_STATE_PVP))
        str2 << " | COMBAT_STATE_PVP";
    if (HasCombatState(COMBAT_STATE_RESET))
        str2 << " | COMBAT_STATE_RESET";
    str2 << ')';
    ch.PSendSysMessage(str2.str().c_str());

    std::ostringstream str3;
    str3 << "3) MovementFlags: (";
    if (HasMovementFlag(MOVEMENT_FLAG_TARGET_CHANGED))
        str3 << " | MOVEMENT_FLAG_TARGET_CHANGED";
    if (HasMovementFlag(MOVEMENT_FLAG_HOLD_GROUND))
        str3 << " | MOVEMENT_FLAG_HOLD_GROUND";
    if (HasMovementFlag(MOVEMENT_FLAG_RANDOM_MOVEMENT))
        str3 << " | MOVEMENT_FLAG_RANDOM_MOVEMENT";
    str3 << ')';
    ch.PSendSysMessage(str3.str().c_str());

    std::ostringstream str4;
    str4 << "4) My spec: " << _mySpec;
    ch.PSendSysMessage(str4.str().c_str());

    std::ostringstream str5;
    str5 << "5) Damage school flags: ";
    uint32 mask = _classAI->GetDamageSchoolMask();
    if (mask & BOT_DAMAGE_SCHOOL_MASK_NORMAL)
        str5 << "BOT_DAMAGE_SCHOOL_MASK_NORMAL";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_RANGED)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_RANGED";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_FIRE)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_FIRE";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_NATURE)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_NATURE";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_FROST)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_FROST";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_SHADOW)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_SHADOW";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_ARCANE)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_ARCANE";
    if (mask & BOT_DAMAGE_SCHOOL_MASK_HOLY)
        str5 << " | BOT_DAMAGE_SCHOOL_MASK_HOLY";
    ch.PSendSysMessage(str5.str().c_str());

    std::ostringstream str6;
    uint32 rolemask = _classAI->GetRoleMask();
    str6 << "6) Roles: ";
    if (rolemask & ROLE_DAMAGE_MELEE)
        str6 << "ROLE_DAMAGE_MELEE";
    if (rolemask & ROLE_DAMAGE_RANGED)
        str6 << " | ROLE_DAMAGE_RANGED";
    if (rolemask & ROLE_DAMAGE_CASTER)
        str6 << " | ROLE_DAMAGE_CASTER";
    if (rolemask & ROLE_DAMAGE_MISC)
        str6 << " | ROLE_DAMAGE_MISC";
    if (rolemask & ROLE_HEAL)
        str6 << " | ROLE_HEAL";
    if (rolemask & ROLE_SUPPORT)
        str6 << " | ROLE_SUPPORT";
    if (rolemask & ROLE_CC)
        str6 << " | ROLE_CC";
    if (rolemask & ROLE_CC_SUPPORT)
        str6 << " | ROLE_CC_SUPPORT";
    if (rolemask & ROLE_TANK)
        str6 << " | ROLE_TANK";
    ch.PSendSysMessage(str6.str().c_str());

    std::ostringstream str7;
    str7 << "7) Positioning flags: ";
    if (_positioningFlags & POSITIONING_NONE)
        str7 << "POSITIONING_NONE";
    if (_positioningFlags & POSITIONING_FOLLOW)
        str7 << " | POSITIONING_FOLLOW";
    if (_positioningFlags & POSITIONING_ATTACK)
        str7 << " | POSITIONING_ATTACK";
    if (_positioningFlags & POSITIONING_TANKING)
        str7 << " | POSITIONING_TANKING";
    if (_positioningFlags & POSITIONING_MELEE)
        str7 << " | POSITIONING_MELEE";
    if (_positioningFlags & POSITIONING_RANGED)
        str7 << " | POSITIONING_RANGED";
    if (_positioningFlags & POSITIONING_KITING)
        str7 << " | POSITIONING_KITING";
    if (_positioningFlags & POSITIONING_LOOTING)
        str7 << " | POSITIONING_LOOTING";
    ch.PSendSysMessage(str7.str().c_str());

    std::ostringstream str8;
    str8 << "Owner (by _ownerGUID): ";
    Player* player = sObjectAccessor->FindPlayer(_ownerGUID);
    if (player)
        str8 << player->GetName() << " (" << uint32(player->GetGUIDLow()) << ')';
    else
        str8 << "unknown (" << uint32(GUID_LOPART(_ownerGUID)) << ')';
    ch.PSendSysMessage(str8.str().c_str());

    std::ostringstream str9;
    str9 << "AI master (by _mgr): ";
    player = GetMaster();
    if (player)
        str9 << player->GetName() << " (" << uint32(player->GetGUIDLow()) << ')';
    else
        str9 << "unknown (NOT FOUND)";
    ch.PSendSysMessage(str9.str().c_str());

    std::ostringstream str10;
    str10 << "Outrun distance: " << uint32(_outrunDistance);
    ch.PSendSysMessage(str10.str().c_str());

    //ch.PSendSysMessage("Listing real spells:");
    //PlayerSpellMap const& spellMap = me->GetSpellMap();
    //for (PlayerSpellMap::const_iterator itr = spellMap.begin(); itr != spellMap.end(); ++itr)
    //{
    //    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(itr->first);
    //    if (!spellInfo)
    //        continue;
    //    PlayerSpell* spell = itr->second;
    //    if (!spell)
    //        continue;
    //    if (spell->disabled || spell->state == PLAYERSPELL_REMOVED || spell->state == PLAYERSPELL_TEMPORARY)
    //        continue;
    //    //triggered or server-side spells
    //    if (spellInfo->SpellLevel == 0)
    //        continue;
    //    //passive spell
    //    if (spellInfo->IsPassive())
    //        continue;
    //    //spell is learned as other class spell
    //    if (!me->IsSpellFitByClassAndRace(spellInfo->Id))
    //        continue;
    //    //is talent
    //    if (GetTalentSpellPos(spellInfo->Id))
    //        continue;
    //    //not hightest rank known
    //    if (spellInfo->GetNextRankSpell() && me->HasSpell(spellInfo->GetNextRankSpell()->Id))
    //        continue;
    //    ////check class family for spells
    //    //ChrClassesEntry const* classEntry = sChrClassesStore.LookupEntry(me->getClass());
    //    //if (spellInfo->SpellFamilyName != classEntry->spellfamily)
    //    //    continue;

    //    std::ostringstream str5;
    //    str5 << spellInfo->Id << " - |cffffffff|Hspell:" << spellInfo->Id << "|h[" << spellInfo->SpellName[loc];
    //    str5 << ' ' << localeNames[loc] << "]|h|r";
    //    uint32 rank = !spellInfo->ChainEntry ? 0 : spellInfo->GetRank();
    //    if (rank > 0)
    //        str5 << " Rank " << rank;
    //    ch.PSendSysMessage(str5.str().c_str());
    //}

    //ch.PSendSysMessage("Listing classAI spells:");
    //_classAI->SendClassDebugInfo();

    //ch.PSendSysMessage("Listing bot instance binds:");

    //for (uint8 i = 0; i != MAX_DIFFICULTY; ++i)
    //{
    //    Player::BoundInstancesMap &binds = me->GetBoundInstances(Difficulty(i));
    //    for (Player::BoundInstancesMap::const_iterator itr = binds.begin(); itr != binds.end(); ++itr)
    //        if (InstanceSave* save = itr->second.save)
    //            ch.PSendSysMessage("map: %d inst: %d perm: %s diff: %d canReset: %s", itr->first, save->GetInstanceId(), itr->second.perm ? "yes" : "no",  save->GetDifficulty(), save->CanReset() ? "yes" : "no");
    //}

    //ch.PSendSysMessage("Listing player instance binds:");

    //for (uint8 i = 0; i != MAX_DIFFICULTY; ++i)
    //{
    //    Player::BoundInstancesMap &binds = _botBoundInstances[Difficulty(i)];
    //    for (Player::BoundInstancesMap::const_iterator itr = binds.begin(); itr != binds.end(); ++itr)
    //        if (InstanceSave* save = itr->second.save)
    //            ch.PSendSysMessage("map: %d inst: %d perm: %s diff: %d canReset: %s", itr->first, save->GetInstanceId(), itr->second.perm ? "yes" : "no",  save->GetDifficulty(), save->CanReset() ? "yes" : "no");
    //}
}

inline void PlayerbotAI::_doTimers(uint32 diff)
{
    if (_waitTimer > diff)                              _waitTimer -= diff;
    if (_followTimer > diff)                            _followTimer -= diff;
    if (_mountTimer > diff)                             _mountTimer -= diff;
    if (_cureTimer > diff)                              _cureTimer -= diff;
    if (_selfResTimer > diff)                           _selfResTimer -= diff;
    if (_sheathTimer > diff)                            _sheathTimer -= diff;
    if (_moveTimer > diff)                              _moveTimer -= diff;
}
