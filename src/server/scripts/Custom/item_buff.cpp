/*
* Made By Sinistah
*/

/*
-- Excute This Sql Into World Database For The Item
INSERT INTO `item_template` VALUES (90001, 0, 0, 0, 'Buff Stone', 46787, 6, 0, 0, 1, 0, 0, 0, -1, -1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1000, 0, 0, 18282, 0, 0, 0, 8000, 0, -1, 0, 0, NULL, 0, -1, 0, -1, 0, 0, NULL, 0, -1, 0, -1, 0, 0, NULL, 0, -1, 0, -1, 0, 0, NULL, 0, -1, 0, -1, 0, '|cff00FF00Use: Buff Yourself|r', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 'item_buff', 0, 0, 0, 0, 0, -4);
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Spell.h"

const uint32 aura[] = { 45525, 48469, 56525, 48169, 48161, 48073 };

class item_buff : public ItemScript
{
public:
    item_buff() : ItemScript("item_buff") { }
 
    bool OnUse(Player* player, Item* item, const SpellCastTargets &)
    {
		if (player->isInCombat())
		{	
			player->GetSession()->SendNotification("You are in combat.");
			return false;
		}
		player->RemoveAurasByType(SPELL_AURA_MOUNTED);
		for(int i = 0; i < sizeof(aura)/sizeof(uint32); i++)
			player->AddAura(aura[i], player);
		player->GetSession()->SendNotification("You are now buffed!");
		return true;
    }
};

void AddSC_item_buff()
{
    new item_buff();
}