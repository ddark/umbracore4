USE `uc2_characters`;
DROP TABLE IF EXISTS `bounties`;

CREATE TABLE `bounties` (
  `guid` INT(200) UNSIGNED NOT NULL,
  `visual` VARCHAR(200) NOT NULL,
  `price` INT(200) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;