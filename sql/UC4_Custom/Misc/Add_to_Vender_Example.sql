-- Add items to Vender by NPC ID

SET
@NPCENTRYID := 'NPC Entry ID',
@ITEM := 'Item ID',
@AMOUNT := 'Amount Sold, 0 is unlimited';

INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`) VALUES 
(@NPCENTRYID, 0, @ITEM, @AMOUNT, 0, 0);