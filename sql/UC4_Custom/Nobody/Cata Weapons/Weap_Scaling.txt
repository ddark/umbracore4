
Level 40:
Armor - Caster 	250
Armor - Melee 	550
Block - Caster	0
Block - Melee	50 

Primary 		200
Secondary 		100
Haste/Crit		50 
SP				250
Expertise		10
Hit				10
Dodge			10
Parry			10	
Block			10

Level 60:
Armor - Caster 	300
Armor - Melee 	600
Block - Caster	0
Block - Melee	60 

Primary 		225
Secondary 		125
Haste/Crit		60 
SP				275
Expertise		12
Hit				15
Dodge			15
Parry			15	
Block			12

Level 80:
Armor - Caster 	350
Armor - Melee 	700
Block - Caster	0
Block - Melee	70 

Primary 		250
Secondary 		150
Haste/Crit		75 
SP				300
Expertise		14
Hit				20
Dodge			20
Parry			20	
Block			14

Level 100:
Armor - Caster 	400
Armor - Melee 	800
Block - Caster	0
Block - Melee	100 

Primary 		300
Secondary 		200
Haste/Crit		100 
SP				450
Expertise		20
Hit				30
Dodge			30
Parry			30	
Block			50

Level 120:
Armor - Caster 	450
Armor - Melee 	900
Block - Caster	0
Block - Melee	125 

Primary 		325
Secondary 		225
Haste/Crit		125 
SP				475
Expertise		20
Hit				35
Dodge			45
Parry			45	
Block			75

Level 140:
Armor - Caster 	500
Armor - Melee 	1025
Block - Caster	0
Block - Melee	125 

Primary 		375
Secondary 		265
Haste/Crit		75 
SP				525
Expertise		25
Hit				40
Dodge			55
Parry			55	
Block			85

Level 180:
Armor - Caster 	550
Armor - Melee 	1200
Block - Caster	0
Block - Melee	155 

Primary 		400
Secondary 		300
Haste/Crit		100 
SP				575
Expertise		25
Hit				45
Dodge			65
Parry			65	
Block			95

Level 200:
Armor - Caster 	600
Armor - Melee 	1350
Block - Caster	0
Block - Melee	175 

Primary 		450
Secondary 		325
Haste/Crit		125 
SP				600
Expertise		28
Hit				50
Dodge			85
Parry			85	
Block			115

Level 220:
Armor - Caster 	650
Armor - Melee 	1500
Block - Caster	0
Block - Melee	185 

Primary 		475
Secondary 		355
Haste/Crit		155 
SP				635
Expertise		30
Hit				55
Dodge			100
Parry			100	
Block			125

Level 240:
Armor - Caster 	675
Armor - Melee 	1600
Block - Caster	0
Block - Melee	195 

Primary 		500
Secondary 		375
Haste/Crit		175 
SP				675
Expertise		30
Hit				65
Dodge			125
Parry			125	
Block			150

Level 255:
Armor - Caster 	750
Armor - Melee 	1800
Block - Caster	0
Block - Melee	250 

Primary 		1000
Secondary 		750
Haste/Crit		250 
SP				1200
Expertise		40
Hit				85
Dodge			250
Parry			250	
Block			300
