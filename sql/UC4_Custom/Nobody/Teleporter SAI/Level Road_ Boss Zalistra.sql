
/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Zalistra', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '5264.509', '-2158.510', '1259.329', '4.720', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
