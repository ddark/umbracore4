REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (100600, 0, 0, 0, 0, 0, 24942, 0, 0, 0, 'Mordac', 'Stormbringer', '', 0, 82, 82, 2, 16, 16, 0, 1, 1.5, 6, 3, 2000, 2480, 0, 300, 6, 2000, 0, 1, 0, 2048, 8, 0, 0, 0, 0, 0, 21, 30, 4, 4, 0, 0, 0, 0, 50, 50, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5000000, 5000000, 'SmartAI', 0, 1, 1, 115, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 618348543, 0, '', 12340);

DELETE FROM `creature_text` WHERE `entry`=100600;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100600,1,0,'TIME TO DIE!!!!',14,0,100,1,0,17457,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100600,2,0,'Crackle! Burn! Sizzle! Scream! AND DIE!!!',14,0,100,1,0,17458,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100600,3,0,'Fools! I control the powers of nature and spirit!',14,0,100,1,0,17459,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100600,4,0,'No!  I die... How can this be?',14,0,100,1,0,17460,'Comment');

SET @ENTRY := 100600;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,20,1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"On aggro do auto-attack."),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,60000,60000,60000,60000,75,39067,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Lightning Shield(39067) every 60 seconds."),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,12000,12000,12000,12000,11,59684,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Flame Shock(59684) every 12 seconds."),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,20000,20000,20000,20000,11,8177,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Grounding Totem(8177) every 20 seconds."),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,7000,7000,7000,7000,11,59519,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Lava Burst(59519) every 7 seconds."),
(@ENTRY,@SOURCETYPE,5,0,0,0,100,0,4000,4000,4000,4000,11,69970,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Lightning Bolt(69970) every 4 seconds."),
(@ENTRY,@SOURCETYPE,6,0,2,0,100,1,74,75,1,1,11,59159,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 75% cast Thunderstorm(59159)."),
(@ENTRY,@SOURCETYPE,7,0,2,0,100,1,49,50,1,1,11,59159,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 50% cast Thunderstorm(59159)."),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,1,24,25,1,1,11,59159,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 25% cast Thunderstorm(59159)."),
(@ENTRY,@SOURCETYPE,9,0,2,0,100,1,34,35,1,1,75,2825,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 35% cast Bloodlust(2825)."),
(@ENTRY,@SOURCETYPE,10,0,2,0,100,1,33,34,1,1,11,51533,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 34% cast Feral Spirit(51533)."),
(@ENTRY,@SOURCETYPE,11,0,0,0,100,0,15000,15000,15000,15000,11,59273,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Chain Lightning(59273) every 15 seconds."),
(@ENTRY,@SOURCETYPE,12,0,0,0,100,0,18000,18000,18000,18000,11,61569,1,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Healing Wave(61569) every 18 seconds."),
(@ENTRY,@SOURCETYPE,13,0,2,0,100,1,24,25,1,1,11,2062,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 25% cast Earth Elemental Totem(2062)."),
(@ENTRY,@SOURCETYPE,14,0,2,0,100,1,24,25,1,1,11,2894,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 25% cast Fire Elemental Totem(2894)."),
(@ENTRY,@SOURCETYPE,15,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,16,0,1,0,100,1,10000,10000,10000,10000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");
(@ENTRY,@SOURCETYPE,17,0,2,0,100,1,60,95,0,0,1,1,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 1"),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,1,40,55,0,0,1,2,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 2"),
(@ENTRY,@SOURCETYPE,19,0,2,0,100,1,20,30,0,0,1,3,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 3"),
(@ENTRY,@SOURCETYPE,20,0,2,0,100,1,1,5,0,0,1,4,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 4"),
