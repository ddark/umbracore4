-- Druid
SET @ENTRY := 80000;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,1,0,0,0,0,11,24858,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Mookin Form(24858) on aggro."),
(@ENTRY,@SOURCETYPE,1,0,23,0,100,1,24858,1,1,5,11,65857,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Cast Entangling Roots(65857) when get Moonkin form."),
(@ENTRY,@SOURCETYPE,2,0,1,0,100,0,0,0,0,0,28,24858,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Removes Moonkin Form(24858) when out of combat."),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,1,93,95,1,5,11,53201,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Cast Starfall(53201) when get to 95% of hp."),
(@ENTRY,@SOURCETYPE,4,0,2,0,100,0,85,93,1,50,11,48465,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Casting from 93% to 85% Starfire(48465)."),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,0,77,84,1,50,11,67952,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Casting from 84% to 77% Wrath(67952)."),
(@ENTRY,@SOURCETYPE,6,0,2,0,100,1,93,95,1,10,11,61384,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Casting on 95% Typhoon(61384)."),
(@ENTRY,@SOURCETYPE,7,0,2,0,100,0,75,77,1,50,11,48463,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Casting from 77% to 75% Moonfire(48463). RAGETIME"),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,1,74,74,1,10,28,24858,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 74% He remove Moonkin Form(24858)."),
(@ENTRY,@SOURCETYPE,9,0,2,0,100,1,70,73,1,10,11,768,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 73% He morph into Cat Form(768)."),
(@ENTRY,@SOURCETYPE,10,0,1,0,100,1,0,0,0,0,28,768,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Removes Cat Form(768) when out of combat."),
(@ENTRY,@SOURCETYPE,11,0,2,0,100,0,65,72,1,100,11,48566,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 72%-65% do MangleCat(48566)"),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,1,70,72,1,100,11,48574,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Cast Rake(48574) at 72%."),
(@ENTRY,@SOURCETYPE,13,0,2,0,100,1,69,70,1,100,11,27008,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 70% cast Rip(27008)."),
(@ENTRY,@SOURCETYPE,14,0,2,0,100,1,55,59,1,1,28,768,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Remove Cat Form(768) at 59%."),
(@ENTRY,@SOURCETYPE,15,0,2,0,100,1,55,59,1,1,11,9634,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Dire Bear Form(9634) 59%."),
(@ENTRY,@SOURCETYPE,16,0,1,0,100,1,1,1,1,1,28,9634,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Dire Bear Form(9634) remove when out of combat."),
(@ENTRY,@SOURCETYPE,17,0,2,0,100,0,35,58,1,100,11,48568,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"58% - 35% Lacerate(48568)."),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,0,35,58,1,100,11,48480,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"58% - 35% Maul(48480). RAGE TIME"),
(@ENTRY,@SOURCETYPE,19,0,2,0,100,1,54,55,1,1,11,48560,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 55% cast Demoralizing Roar(48560)."),
(@ENTRY,@SOURCETYPE,20,0,2,0,100,1,54,55,1,100,11,48564,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 55% casting MangleBear(48564)."),
(@ENTRY,@SOURCETYPE,21,0,2,0,100,1,49,50,1,100,11,8983,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 50% casting Bash(8983)."),
(@ENTRY,@SOURCETYPE,22,0,2,0,100,1,44,45,1,100,11,8983,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 45% casting Bash(8983)."),
(@ENTRY,@SOURCETYPE,23,0,2,0,100,1,35,36,1,100,11,8983,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 36% casting Bash(8983)."),
(@ENTRY,@SOURCETYPE,24,0,2,0,100,1,20,35,1,1,28,9634,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 35% removes Dire Bear Form."),
(@ENTRY,@SOURCETYPE,25,0,2,0,100,1,31,33,1,1,11,71121,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Healing Touch(71121) at 33% of hp."),
(@ENTRY,@SOURCETYPE,26,0,2,0,100,1,28,30,1,1,11,768,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 30% Get cat form til death."),
(@ENTRY,@SOURCETYPE,27,0,2,0,100,0,1,30,1,100,11,48566,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 30%-1% do MangleCat(48566)"),
(@ENTRY,@SOURCETYPE,28,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,29,0,1,0,100,1,10000,10000,1,1,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");