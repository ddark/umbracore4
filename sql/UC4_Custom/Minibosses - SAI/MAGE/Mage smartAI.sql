-- Mage
SET @ENTRY := 80003;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,1,0,4,0,100,1,0,0,0,0,11,1953,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"On aggro Blink(1953) to target."),
(@ENTRY,@SOURCETYPE,2,0,2,0,100,1,48,99,1,1,22,1,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: 99%-48%."),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,1,73,74,1,1,11,61025,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase switch: Polymorph(61025)."),
(@ENTRY,@SOURCETYPE,4,0,2,0,100,1,46,47,1,1,11,61780,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase switch: Polymorph(61780)."),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,1,19,20,1,1,11,29963,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase switch: Mass Polymorph(29963)."),
(@ENTRY,@SOURCETYPE,6,0,2,0,100,1,98,99,1,1,11,43046,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Molten Armor(43046)."),
(@ENTRY,@SOURCETYPE,7,0,0,1,100,0,26000,26000,26000,26000,11,29964,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Dragon�s Breath(29964) every 26 seconds."),
(@ENTRY,@SOURCETYPE,8,0,0,1,100,0,25000,25000,25000,25000,11,61568,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Flamestrike(61568) every 25 seconds."),
(@ENTRY,@SOURCETYPE,9,0,0,1,100,0,12000,12000,12000,12000,11,55362,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Living Bomb(55360) every 12 seconds."),
(@ENTRY,@SOURCETYPE,10,0,0,1,25,0,7000,7000,7000,7000,11,64698,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Pyroblast(64698) every 4 seconds (25% chance to hit)."),
(@ENTRY,@SOURCETYPE,11,0,0,1,100,0,3000,3000,3000,3000,11,31262,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Fireball(31262) every 3 seconds."),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,1,1,45,1,1,22,2,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: 45%-1%."),
(@ENTRY,@SOURCETYPE,13,0,2,0,100,1,43,44,1,1,11,29718,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Elemental Armor(29718)."),
(@ENTRY,@SOURCETYPE,14,0,2,2,100,1,34,35,1,1,11,43039,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: At 35% cast Ice Barrier(43039)."),
(@ENTRY,@SOURCETYPE,15,0,0,2,100,0,15000,15000,15000,15000,11,42940,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Blizzard(42940) every 15 seconds."),
(@ENTRY,@SOURCETYPE,16,0,0,2,100,0,7000,7000,7000,7000,11,42842,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Frostbolt(42842) every 7 seconds."),
(@ENTRY,@SOURCETYPE,17,0,0,2,100,0,4000,4000,4000,4000,11,42914,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Ice Lance(42914) every 4 seconds."),
(@ENTRY,@SOURCETYPE,18,0,0,2,100,0,10000,10000,10000,10000,11,69869,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Frostfire Bolt(69869) every 10 seconds."),
(@ENTRY,@SOURCETYPE,19,0,0,2,100,0,30000,30000,30000,30000,11,65802,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Iceblock(65802) every 30 seconds."),
(@ENTRY,@SOURCETYPE,20,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,21,0,1,0,100,1,10000,10000,10000,10000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat."),
(@ENTRY,@SOURCETYPE,22,0,0,0,100,0,10000,10000,10000,10000,75,54919,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Debuffing real Mage with Warped Armor(54919)."),
(@ENTRY,@SOURCETYPE,23,0,2,0,100,1,82,84,1,1,12,37130,4,10000,0,1,0,0,0,0,0,0.0,0.0,0.0,0.0,"Summon Mirror Mage(37130).");