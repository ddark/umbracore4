-- Paladin
SET @ENTRY := 80009;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,11,20164,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Seal of Justice(20164)."),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,20000,20000,20000,20000,11,43429,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Consecration(43429) every 20 seconds."),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,12000,12000,12000,12000,11,58822,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Exorcism(58822) every 12 seconds."),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,60000,60000,60000,60000,11,66112,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Holy Light(66112) every 60 seconds."),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,4000,4000,4000,4000,11,71117,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Avenger�s Shield(71117) every 4 seconds."),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,1,49,50,1,1,11,642,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Divine Shield(642) at 50%."),
(@ENTRY,@SOURCETYPE,6,0,0,0,100,0,18000,18000,18000,18000,11,10308,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Hammer of Justice(10308) every 18 seconds."),
(@ENTRY,@SOURCETYPE,7,0,0,0,100,0,22000,22000,22000,22000,11,31904,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Holy Shield(31904) every 22 seconds."),
(@ENTRY,@SOURCETYPE,8,0,0,0,100,0,6000,6000,6000,6000,11,46033,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Judgement of Wrath(46033) every 6 seconds."),
(@ENTRY,@SOURCETYPE,9,0,0,0,100,0,29000,29000,29000,29000,11,31884,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Avenging Wrath(31884) every 29 seconds."),
(@ENTRY,@SOURCETYPE,10,0,12,0,100,0,1,20,4000,4000,11,37255,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Hammer of Wrath(37255) when target is >20% 4sec CD."),
(@ENTRY,@SOURCETYPE,12,0,0,0,100,0,7000,7000,7000,7000,11,66903,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Hammer of the Righteous(66903) every 7 seconds."),
(@ENTRY,@SOURCETYPE,13,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,14,0,1,0,100,1,5000,5000,5000,5000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");