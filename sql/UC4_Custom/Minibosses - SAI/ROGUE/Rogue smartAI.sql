-- Rogue
SET @ENTRY := 80008;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,1,0,100,1,1000,1000,1000,1000,11,1787,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Stealth(1787)."),
(@ENTRY,@SOURCETYPE,1,0,4,0,100,0,0,0,0,0,11,72327,2,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Shadowstep(72327)."),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,30000,30000,30000,30000,11,2094,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Blind(2094) every 59 seconds."),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,59600,59600,59600,59600,11,26889,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Vanish(26889) every 60 seconds."),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,4000,4000,4000,4000,11,59409,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Sinister Strike(59409) every 4 seconds."),
(@ENTRY,@SOURCETYPE,5,0,13,0,100,1,1,1,0,0,11,31224,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Cloak of Shadows(31224) when target is casting(only once)."),
(@ENTRY,@SOURCETYPE,6,0,0,0,100,0,45000,45000,45000,45000,11,57842,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Killing Spree(57842) every 45 seconds."),
(@ENTRY,@SOURCETYPE,7,0,0,0,100,0,62000,62000,62000,62000,11,26669,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Evasion(26669) every 62 seconds."),
(@ENTRY,@SOURCETYPE,8,0,0,0,100,0,40000,40000,40000,40000,11,13877,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Blade Flurry(13877) every 40 seconds."),
(@ENTRY,@SOURCETYPE,9,0,67,0,100,0,2000,2000,0,0,11,34614,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"When behing the target cast Backstab(34614)."),
(@ENTRY,@SOURCETYPE,10,0,0,0,100,0,32000,32000,32000,32000,11,6774,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Slice and Dice(6774) every 32 seconds."),
(@ENTRY,@SOURCETYPE,11,0,0,0,100,0,9000,9000,9000,9000,11,32864,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Kidney Shot(32864) every 9 seconds."),
(@ENTRY,@SOURCETYPE,12,0,0,0,100,0,6000,6000,6000,6000,11,41487,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Envenom(41487) every 6 seconds."),
(@ENTRY,@SOURCETYPE,13,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset on death."),
(@ENTRY,@SOURCETYPE,14,0,1,0,100,1,5000,5000,5000,5000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset when out of combat.");