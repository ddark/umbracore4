-- Death Knight
SET @ENTRY := 80006;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,20,1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"On aggro do auto-attack."),
(@ENTRY,@SOURCETYPE,1,0,4,0,100,1,0,0,0,0,11,48266,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Blood Presence(48266)."),
(@ENTRY,@SOURCETYPE,2,0,4,0,100,1,0,0,0,0,11,55222,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Unholy Presence(55222)."),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,60000,60000,60000,60000,11,49005,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Mark of Blood(49005) every 60 seconds."),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,4000,4000,4000,4000,11,55978,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Heart Strike(55978) every 4 seconds."),
(@ENTRY,@SOURCETYPE,5,0,0,0,100,0,12000,12000,12000,12000,11,53639,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death Strike(53639) every 12 seconds."),
(@ENTRY,@SOURCETYPE,6,0,0,0,100,0,6000,6000,6000,6000,11,66019,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death Coil(66019) every 6 seconds."),
(@ENTRY,@SOURCETYPE,7,0,0,0,100,0,45000,45000,45000,45000,11,49203,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Hungering Cold(49203) every 45 seconds."),
(@ENTRY,@SOURCETYPE,8,0,0,0,100,0,46000,46000,46000,46000,11,61061,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Howling Blast(61061) every 46 seconds."),
(@ENTRY,@SOURCETYPE,9,0,0,0,100,0,30000,30000,30000,30000,11,72434,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Obliterate(72434) every 30 seconds."),
(@ENTRY,@SOURCETYPE,10,0,13,0,100,0,1,1,0,0,11,51052,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Anti-Magic Zone(51052) when target is casting."),
(@ENTRY,@SOURCETYPE,11,0,0,0,100,0,10000,10000,10000,10000,11,56359,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death and Decay(56359) every 10 seconds."),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,1,98,99,1,1,12,5,4,10000,0,1,0,5,0,0,0,0.0,0.0,0.0,0.0,"On aggro summon Deatchill Servant(5)."),
(@ENTRY,@SOURCETYPE,13,0,0,0,100,0,30000,30000,30000,30000,11,49206,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Summon Gargoyle(49206) every 30 seconds."),
(@ENTRY,@SOURCETYPE,14,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,15,0,1,0,100,1,5000,5000,5000,5000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");