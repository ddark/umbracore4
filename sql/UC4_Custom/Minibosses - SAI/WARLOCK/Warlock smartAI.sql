-- Warlock
SET @ENTRY := 80001;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,1,0,4,0,100,0,0,0,0,0,12,416,4,5000,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"On Aggro summon Imp(416)."),
(@ENTRY,@SOURCETYPE,2,0,4,0,100,1,0,0,0,0,11,25309,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Immolation(25309)."),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,0,90,99,1,50,11,47838,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 99% to 90% Incineration(47838)."),
(@ENTRY,@SOURCETYPE,4,0,2,0,100,1,88,89,1,10,11,47847,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 89% cast Shadowfury(47847)."),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,0,78,88,1,50,11,59172,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 88% to 78% Chaos Bolt(59172)."),
(@ENTRY,@SOURCETYPE,6,0,2,0,100,0,66,77,1,50,11,27209,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 77% to 66% Shadow Bolt(27209)."),
(@ENTRY,@SOURCETYPE,7,0,2,0,100,1,64,65,1,10,11,59164,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 65% Haunt(59164)."),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,1,63,64,1,10,11,47843,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 64% Unstable Affliction(47843)."),
(@ENTRY,@SOURCETYPE,9,0,2,0,100,1,59,60,0,0,11,1122,4,0,0,0,0,4,0,0,0,0.0,0.0,0.0,0.0,"At 60% summoning Inferno(1122)."),
(@ENTRY,@SOURCETYPE,10,0,2,0,100,1,54,55,1,10,11,47241,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 55% Metamorphosis(47241)."),
(@ENTRY,@SOURCETYPE,12,0,23,0,100,1,47241,1,1,1,11,50589,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"If He has Metamorphosis(47241) He cast Immolation Aura(50589)."),
(@ENTRY,@SOURCETYPE,13,0,23,0,100,0,47241,1,1,1,11,50581,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"If He has Metamorphosis(47241) He cast Shadow Cleave(50581)."),
(@ENTRY,@SOURCETYPE,14,0,2,0,100,1,38,40,1,100,11,27223,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 40% cast Death Coil(27223)."),
(@ENTRY,@SOURCETYPE,15,0,2,0,10,0,10,38,1,100,11,47825,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 38%-10% cast Soul Fire(47825). RAGE TIME 10% chance to hit."),
(@ENTRY,@SOURCETYPE,16,0,2,0,100,1,8,9,1,1,11,6215,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 9% cast Fear(6215)."),
(@ENTRY,@SOURCETYPE,17,0,2,0,100,0,1,9,1,100,11,27219,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 9%-1% Drain Life(27219)."),
(@ENTRY,@SOURCETYPE,18,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,19,0,1,0,100,1,10000,10000,1,1,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");